export const ip = "http://192.168.1.128:3434/" // hop office
// export const ip = "http://192.168.137.148:3003/api/v1/" // aom office
// export const ip = "http://192.168.1.96:3434/" //copter
// export const ip = "http://172.20.10.3:3434/" //can
// export const ip = "http://192.168.0.57:3434/" //router
// export const ip = "http://192.168.1.66:3434/" //aom

var jwt_decode = require('jwt-decode');

const decode_token = (user_token_decoded_func) => {
    let decoded
    if (user_token_decoded_func) {
        decoded = jwt_decode(user_token_decoded_func);
    } else {
        decoded = { id: null, type: null }
    }
    return decoded;
}