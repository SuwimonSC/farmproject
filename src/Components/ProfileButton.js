import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

 class ProfileButton extends Component {
  render () {
    return (
        
      <TouchableOpacity
        onPress={() => {
          this.props.onPress ();
        }}
        style={styles.bottonBody}
      >
        <Text styles={styles.bottonText}>
          {this.props.title}
        </Text>
      </TouchableOpacity>
      
    );
  }
}

const styles = StyleSheet.create ({
  bottonBody: {
    width: '100%',
    width: 300,
    height: 40,
    backgroundColor: '#2C8D5D',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    fontSize: 200,
    borderRadius:150
  },
  bottonText: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#CE26F8',
    //  fontSize: 200,
  },
});

export {ProfileButton};
