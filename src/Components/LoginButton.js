
import React, {Component} from 'react';
import { Text, View, StyleSheet,TouchableOpacity} from 'react-native'

export default class LoginButton extends Component  {
    render () {
      return (
         
         <TouchableOpacity onPress={()=>{this.props.onPress()}} style={styles.buttonBody}>
           <Text styles={styles.buttonText}>
                  {this.props.title}
           </Text>
         </TouchableOpacity>  
         
      )
    }
 }

 const styles = StyleSheet.create ({
    buttonBody:{
       width:'100%',
       width:300,
       height:40,
       borderRadius:10,
       backgroundColor:"#009CF2",
       justifyContent:'center',
       alignItems:'center',
       marginTop:10, 
    },
    buttonText : {
       alignItems:'center',
       justifyContent:'center',
       backgroundColor:"#CE26F8",
       
      
    },
    
 });

 export {LoginButton};