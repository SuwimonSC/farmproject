import React, {Component} from 'react';
import io from 'socket.io-client';
import {
  View,
  Text,
  Alert,
  Image,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Card, TextInput} from 'react-native-paper';
import {get, post, Delete, put} from '../Support/service';
import AsyncStorage from '@react-native-community/async-storage';
import Icons from 'react-native-vector-icons/FontAwesome';
import {DataTable, Switch} from 'react-native-paper';
import {Header} from 'native-base';

class DeviceType extends Component {
  constructor (props) {
    super (props);
    this.state = {
      device_type_array: [],
    };
  }

  componentDidMount () {
    this.get_Device_type ();
  }
  get_Device_type = async () => {
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        get (`api/v1/device/device_type`, user_token).then (res => {
          if (res.success) {
            // console.log ('res', res.result);
            this.setState (
              {
                device_type_array: res.result,
              },
              () => {
                // console.log (' : ', this.state.);
              }
            );
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };
  render () {
    return (
      <View>
        <Header style={{backgroundColor: '#DDEAFF',borderColor:'#2866CD',borderBottomWidth:1}}>
          <Text style={{marginTop: 15, fontSize: 18}}>ประเภทอุปกรณ์</Text>
        </Header>
        {this.state.device_type_array.map (element => {
          return (
            <Card style={styles.cardStyle}>
              <Text
                style={{fontSize: 18, margin: 15,textAlign:'left', paddingLeft:15}}
                onPress={() => this.props.navigation.navigate ('DeviceData',{screen:'DeviceData',params:{data : element.device_type}})}
                 > 
                {element.device_type}
              </Text>
            </Card>
          );
        })}

      </View>
    );
  }
}
export default DeviceType;

const styles = StyleSheet.create ({
  buttonSave: {
    backgroundColor: '#2C8D5D',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 100,
    justifyContent: 'center',
    left: 170,
  },
  buttonTextSave: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 18,
  },
  // cardStyle: {
  //   height: 100,
  //   marginBottom: 5,
  //   backgroundColor: 'transparent',
  //   borderTopRightRadius:10,
    
  //   display:'flex',
  //   marginVertical:'auto',
    
  //   width: '80%',

  //   // left: 30,
  //   borderColor:'#97BCF8',
  //   borderLeftWidth:5,
  //   borderRightWidth:0,
  // },
  cardStyle: {
    borderColor:'#97BCF8',
    borderLeftWidth:10,
    marginRight:15,
    marginLeft:15,
    marginTop:10,
  },
});
