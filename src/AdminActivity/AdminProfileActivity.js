import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TextInput} from 'react-native';
import {Container, Header, Card, CardItem, Body} from 'native-base';
import {ProfileButton} from '../Components/ProfileButton';
import {get, post, Delete, put} from '../Support/service';
import AsyncStorage from '@react-native-community/async-storage';
import Icons from 'react-native-vector-icons/FontAwesome';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';

const user_token = AsyncStorage.getItem ('user_token');

export default class AdminProfileActivity extends Component {
  constructor () {
    super ();
    this.state = {
      user_data: null,
      edit: false,
      data_cancel: null,
    };
  }
  componentDidMount () {
    this.get_profile ();
  }

  logOut = () => {
    // AsyncStorage.getItem ('user_token')
    AsyncStorage.removeItem ('user_token').then (result => {
      this.setState ({
        user_token: result,
      });
      this.props.navigation.navigate ('Login');
    });
  };

  get_profile = async () => {
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        get (`api/v1/user/user_data`, user_token).then (res => {
          if (res.success) {
            // console.log ('res : ', res.result);
            this.setState (
              {
                user_data: res.result[0],
              },
              () => {
                console.log ('user_data:', this.state.user_data);
              }
            );
          } else {
            alert ('get_user', res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };
  onEdit = () => {
    let data_cancel = JSON.stringify (this.state.user_data);
    this.setState ({
      edit: !this.state.edit,
      data_cancel: data_cancel,
    });
  };
  onEditCancel = () => {
    let data_cancel = JSON.parse (this.state.data_cancel);
    this.setState ({
      edit: !this.state.edit,
      user_data: data_cancel,
    });
  };

  onEditSave = () => {
    this.setState (
      {
        edit: !this.state.edit,
      },
      () => {
        this.onEditSaveDatabase ();
      }
    );
  };

  onEditSaveDatabase = async () => {
    let obj = this.state.user_data;
    // console.log (obj);
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        put (obj, `api/v1/user/edit_user_data`, user_token).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  onEditNameChange = (event) => {
    let name = this.state.user_data
    name.name = event
    this.setState ({
      user_data: name,
    });
  };

  onEditLastNameChange = (event) => {
    let last_name = this.state.user_data
    last_name.last_name = event
    this.setState ({
      user_data: last_name,
    });
  };

  onEditEmailChange = (event) => {
    let email = this.state.user_data
    email.email = event
    this.setState ({
      user_data: email,
    });
  };

  onEditPhoneNumberChange = (event) => {
    let phone_number = this.state.user_data
    phone_number.phone_number = event
    this.setState ({
      user_data: phone_number,
    });
  };

  onEditAddressChange = (event) => {
    let address = this.state.user_data
    address.address = event
    this.setState ({
      user_data: address,
    });
  };


  render () {
    return (
      <View>
        <ScrollView>
          <Header style={{height: 50, backgroundColor: '#2866CD'}} />
          <View style={styles.header} />

          
          {/* <Icons name="upload" style={{fontSize: 18, left: 150, marginTop: 5}}>
            <Text style={{fontSize: 14}}>Upload Picture</Text>
          </Icons> */}

          <View style={styles.body}>

            {!this.state.edit
              ? <Card
                  style={{
                    height:460,
                    bottom: 0,
                    width: 370,
                    left: 20,
                    borderRadius: 20,
                  }}
                >
                <Image
                  style={styles.avatar}
                  source={{
                    uri: 'https://i.pinimg.com/originals/52/e3/de/52e3ded3a54314f8b0eed0083cb395df.png',
                  }}
                />
                  <Icons
                    style={{
                      fontSize: 25,
                      color: '#2866CD',
                      left: 330,
                      margin: 5,
                    }}
                    name="edit"
                    onPress={() => {
                      this.onEdit ();
                    }}
                  />
                  


                  <CardItem>
                    <View style={styles.bodyContent}>
                      <Text style={styles.name}>
                        ข้อมูลส่วนตัว 
                      </Text>

                      <CardItem bordered style={{width: 320}}>
                        <View style={{marginTop: 40}}>

                          <Body>
                            <Text style={{bottom: 20}}>
                              Name :
                              {' '}
                              {this.state.user_data
                                ? this.state.user_data.name
                                : null}
                              {' '}
                              {this.state.user_data
                                ? this.state.user_data.last_name
                                : null}
                            </Text>
                          </Body>
                        </View>
                      </CardItem>
                      <CardItem bordered style={{width: 320}}>
                        <Body>
                          <Text style={{top: 5}}>
                            Email :
                            {' '}
                            {this.state.user_data
                              ? this.state.user_data.email
                              : null}
                          </Text>
                        </Body>
                      </CardItem>
                      <CardItem bordered style={{width: 320}}>
                        <Body>
                          <Text style={{top: 5}}>
                            Phone :
                            {' '}
                            {this.state.user_data
                              ? this.state.user_data.phone_number
                              : null}
                          </Text>
                        </Body>
                      </CardItem>
                      <CardItem bordered style={{width: 320}}>
                        <Body>
                          <Text style={{top: 5}}>
                            Address :
                            {' '}
                            {this.state.user_data
                              ? this.state.user_data.address
                              : null}
                          </Text>
                        </Body>
                      </CardItem>

                      <View style={{top: 20}}>
                        <TouchableOpacity
                          style={styles.buttonSave}
                          title="Log Out"
                          onPress={() => {
                            this.logOut ();
                          }}
                        >
                          <Text style={styles.buttonTextSave}>Logout</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </CardItem>
                </Card>
              : <Card
                  style={{
                    height:460,
                    bottom: 0,
                    width: 370,
                    left: 20,
                    borderRadius: 20,
                  }}
                >
                 <Image
                  style={styles.avatar}
                  source={{
                    uri: 'https://i.pinimg.com/originals/52/e3/de/52e3ded3a54314f8b0eed0083cb395df.png',
                  }}
                />
                  <View>
                    <Icons
                      style={{
                        fontSize: 25,
                        color: '#2866CD',
                        left: 330,
                        margin: 5,
                      }}
                      name="edit"
                      onPress={() => {
                        this.onEdit ();
                      }}
                    />
                    <Icons
                      style={{
                        fontSize: 25,
                        color: 'red',
                        left: 300,
                        bottom: 30,
                      }}
                      name="times-circle"
                      onPress={() => {
                        this.onEditCancel ();
                      }}
                    />
                  </View>
                  <CardItem>
                    <View style={styles.bodyContent}>
                      <CardItem style={{width: 320}}>
                        <View style={{marginTop: 30}}>
                          <Body>
                            <Text style={{bottom: 20}}>
                              Name :{' '}
                            </Text>
                          </Body>
                          <View style={styles.ContainerTextInput}>
                            <TextInput
                              style={{width: 130, left: 20}}
                              placeholder="ชื่อ"
                              maxLength={30}
                              underlineColorAndroid="#C5C5C5"
                              type="text"
                              color="gray"
                              value={this.state.user_data.name}
                              onChangeText={event => this.onEditNameChange (event)}
                            />
                            <TextInput
                              style={{width: 130, right: 15, marginTop: 10}}
                              placeholder="นามสกุล"
                              maxLength={30}
                              underlineColorAndroid="#C5C5C5"
                              type="text"
                              color="gray"
                              value={this.state.user_data.last_name}
                              onChangeText={event => this.onEditLastNameChange (event)}
                            />
                          </View>
                        </View>
                      </CardItem>
                      <View />
                      <CardItem style={{width: 320, bottom: 20}}>
                        <Body>
                          <View />
                          <Text style={{bottom: 20}}>
                            Email :
                          </Text>
                        </Body>
                        <TextInput
                          style={{width: 200, right: 10, bottom: 40}}
                          placeholder="Email"
                          maxLength={30}
                          underlineColorAndroid="#C5C5C5"
                          type="text"
                          color="gray"
                          value={this.state.user_data.email}
                          onChangeText={event => this.onEditEmailChange (event)}
                        />
                      </CardItem>
                      <CardItem style={{width: 320, bottom: 20}}>
                        <Body>
                          <Text style={{bottom: 30}}>
                            Phone :
                          </Text>
                        </Body>
                        <TextInput
                          style={{width: 200, right: 10, bottom: 50}}
                          placeholder="เบอร์โทร"
                          maxLength={30}
                          underlineColorAndroid="#C5C5C5"
                          type="text"
                          color="gray"
                          value={this.state.user_data.phone_number}
                          onChangeText={event => this.onEditPhoneNumberChange (event)}
                        />
                      </CardItem>
                      <CardItem style={{width: 320, bottom: 20}}>
                        <Body>
                          <Text style={{bottom: 30}}>
                            address :
                          </Text>
                        </Body>
                        <TextInput
                          style={{width: 200, right: 10, bottom: 50}}
                          placeholder="ที่อยู่"
                          maxLength={30}
                          underlineColorAndroid="#C5C5C5"
                          type="text"
                          color="gray"
                          value={this.state.user_data.address}
                          onChangeText={event => this.onEditAddressChange (event)}
                        />
                      </CardItem>
                      <View style={{bottom: 50}}>
                        <TouchableOpacity
                          style={styles.buttonSave}
                          title="Log Out"
                          onPress={() => {
                            this.onEditSave ();
                          }}
                        >
                          <Text style={styles.buttonTextSave}>Save</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </CardItem>
                </Card>}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  header: {
    backgroundColor: '#2866CD',
    height: 80,
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 3,
    borderColor: 'white',
    marginBottom: 10,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: -80,
    
  },
  name: {
    fontSize: 22,
    color: '#FFFFFF',
    fontWeight: '600',
  },
  body: {
    marginTop: 10,
  },
  bodyContent: {
    flex: 1,
    alignItems: "center",
    padding: 30,
    marginTop:0,
  },
  name: {
    fontSize: 28,
    color: '#696969',
    fontWeight: '600',
  },

  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
    backgroundColor: '#5D4C78',
  },
  buttonSave: {
    width: 250,
    height: 40,
    backgroundColor: '#2866CD',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 150,
    top:20
  },
  buttonTextSave: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
  },
  ContainerTextInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 300,
    bottom: 50,
    left: 30,
  },
});
