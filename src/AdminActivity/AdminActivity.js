import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  Animated,
  Image,
  ScrollView,
} from 'react-native';
import {DataTable, Switch} from 'react-native-paper';
import {get, post, Delete, put} from '../Support/service';
import Icons from 'react-native-vector-icons/FontAwesome';

class AdimActivity extends Component {
  constructor (props) {
    super (props);
    this.state = {
      user_data: [],
    };
  }

  componentDidMount () {
    this.get_user ();
  }
  get_user = async () => {
    try {
      await get (`api/v1/user/users_data`, null).then (res => {
        if (res.success) {
          this.setState (
            {
              user_data: res.result,
            },
            () => {
              // console.log (' : ', this.state.);
            }
          );
          // console.log ('res', res.result);
        } else {
          alert (res.error_message);
        }
      });
      //   });
    } catch (error) {
      alert (error);
    }
  };
  render () {
    return (
      <View>
        <DataTable style={{marginTop: 0}}>

          <DataTable.Header style={{borderColor: '#71A4F8', borderWidth: 1,backgroundColor:'71A4F8'}}>
            <DataTable.Title style={{left: 20}}>ชื่อ-นามสกุล</DataTable.Title>
            <DataTable.Title style={{left: 20}}>Email</DataTable.Title>
            <DataTable.Title style={{left: 20}}>รายละเอียด</DataTable.Title>
          </DataTable.Header>

          {this.state.user_data.map ((element, index) => {
            console.log ('element : ', element);
            return (
              <ScrollView>
                <DataTable.Row style={styles.DataTableRowStyle}>
                  <DataTable.Cell>
                    {element.name} {element.last_name}
                  </DataTable.Cell>
                  <DataTable.Cell>
                    {element.email}
                  </DataTable.Cell>
                  <Icons
                    style={{
                      fontSize: 25,
                      color: 'black',
                      marginTop: 10,
                      left: 120,
                    }}
                    name="angle-right"
                  />
                  <DataTable.Cell
                    onPress={() => 
                      this.props.navigation.navigate ('SeeMore', { screen: 'SeeMore', params: {data:element}})}
                
                  />

                </DataTable.Row>
              </ScrollView>
            );
          })}

        </DataTable>
      </View>
    );
  }
}
export default AdimActivity;
const styles = StyleSheet.create ({
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    bottom: 50,
  },
  DataTableRowStyle: {
    backgroundColor: '#A8C7FB',
    paddingLeft: 15,
    paddingRight: 15,
    borderColor: '#2866CD',
    borderWidth: 1,
  },
});
