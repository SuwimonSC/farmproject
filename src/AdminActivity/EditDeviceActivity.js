import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  Animated,
  Image,
  ScrollView,
} from 'react-native';
import {DataTable, Switch} from 'react-native-paper';
import {get, post, Delete, put} from '../Support/service';
import Icons from 'react-native-vector-icons/FontAwesome';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

class EditDeviceActivity extends Component {
  constructor (props) {
    super (props);
    this.state = {
      data_device: null,
      data_device_cancel: null,
    };
  }

  componentDidMount () {
    this.setState (
      {
        data_device: this.props.route.params.params.data,
        data_device_cancel: JSON.stringify (
          this.props.route.params.params.data
        ),
      },
      () => {
        // console.log ('data_device : ', this.state.data_device);
      }
    );
  }
  editDevice_name = event => {
    let change_name = this.state.data_device;
    change_name.device_name = event;
    this.setState ({
      data_device: change_name,
    });
  };

  editDevice = () => {
    let obj = this.state.data_device;
    // console.log ('obj : ', obj);

    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        put (obj, 'api/v1/device/devices_data_admin', user_token).then (res => {
          if (res.success) {
            this.props.navigation.navigate ('DeviceData', {
              screen: 'DeviceData',
              params: {data: this.state.data_device.device_type},
            });
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  onback = () => {
    // console.log(JSON.parse(this.state.data_device_cancel))
    let data_cancel = JSON.parse (this.state.data_device_cancel);
    this.setState (
      {
        data_device: JSON.parse (this.state.data_device_cancel),
      },
      () => {
        // console.log (this.state.data_device);
        this.props.navigation.navigate ('DeviceData');
      }
    );
  };

  render () {
    return (
      <View>
        <Icons
          style={{fontSize: 25, margin: 20, color: '#2866CD', width: 50}}
          name="chevron-left"
          onPress={() => this.onback ()}
        />
        {this.state.data_device
        ? <View style={{left: 70}}>
          <Text style={{margin: 5, fontSize: 17}}>
            device_name :
          </Text>
          <TextInput
            style={{width: 250, margin: 5}}
            underlineColorAndroid="#969797"
            value={
              this.state.data_device ? this.state.data_device.device_name : null
            }
            onChangeText={event => {
              this.editDevice_name (event);
            }}
          />
          <Text style={{margin: 5, fontSize: 17}}>
            Client_id :
            {this.state.data_device ? this.state.data_device.client_id : null}
          </Text>

          <Text style={{margin: 5, fontSize: 17}}>
            device_id :
            {this.state.data_device ? this.state.data_device.device_id : null}
          </Text>

          <Text style={{margin: 5, fontSize: 17}}>
            group_id :
            {this.state.data_device.group_id !=null ? this.state.data_device.group_id : 'ไม่มี'}
          </Text>

          <Text style={{margin: 5, fontSize: 17}}>
            user_id ที่กำลังใช้งาน :
            {this.state.data_device.user_id != null ? this.state.data_device.user_id : 'ไม่มี'}                                     
          </Text>
          <TouchableOpacity
            style={styles.buttonSave}
            onPress={() => {
              this.editDevice ();
            }} >        
            <Text style={styles.buttonTextSave}>save</Text>
          </TouchableOpacity>
        </View>
         : null}
      </View>
    );
  }
}
export default EditDeviceActivity;

const styles = StyleSheet.create ({
  buttonSave: {
    backgroundColor: '#2866CD',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 100,
    justifyContent: 'center',
    left: 100,
  },
  buttonTextSave: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 18,
  },
});
