import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  Animated,
  Image,
  ScrollView,
} from 'react-native';
import {Button, Card, DataTable, Switch} from 'react-native-paper';
import {get, post, Delete, put} from '../Support/service';
import Icons from 'react-native-vector-icons/FontAwesome';
import {Header} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';

class UserInfomation extends Component {
  constructor (props) {
    super (props);
    this.state = {
      user_data: null,
      image: '',
      edit: false,
    };
  }
  componentDidMount () {
    this.setState (
      {
        user_data: this.props.route.params.data,
      },
      () => {
        // console.log ('user : ', this.state.user_data);
      }
    );
  }
  render () {
    return (
      <ScrollView>
        <View>

          <Header style={{backgroundColor: '#DDEAFF'}} />
          <Icons
            style={{
              fontSize: 25,
              color: '#2866CD',
              bottom: 40,
              left: 20,
              justifyContent: 'space-between',
              width:30,
            }}
            name="chevron-left"
            onPress={() => this.props.navigation.navigate ('User')}
          />
          <View>
            <Icons
              style={{fontSize: 25, color: '#2866CD', bottom: 60, left: 350}}
              name="edit"
              onPress={() => {
                this.setState ({edit: !this.state.edit});
              }}
            />
          </View>

          <Image
            style={{
              height: 200,
              width: 200,
              borderRadius: 10,
              left: 110,
              bottom: 20,
            }}
            source={require ('../images/Profile-Icon.png')}
          />

          {!this.state.edit
            ? <View style={{marginTop: 5}}>
                <Text style={{left: 50, marginTop: 20}}>
                  <Card style={{width: 30, height: 30, borderRadius: 20}}>
                    <Icons
                      style={{
                        fontSize: 20,
                        color: '#8D94F9',
                        left: 5,
                        margin: 3,
                      }}
                      name="user"
                    />
                  </Card>
                  Name :
                </Text>
                <Text style={{left: 70, fontSize: 16, fontWeight: 'bold'}}>
                  {this.state.user_data ? this.state.user_data.name : null}   {this.state.user_data ? this.state.user_data.last_name : null}
                 </Text>                 
                <Text style={{left: 50, marginTop: 20}}>
                  <Card style={{width: 30, height: 30, borderRadius: 20}}>
                    <Icons
                      style={{
                        fontSize: 20,
                        color: '#E5D402',
                        left: 5,
                        left: 1,
                        margin: 5,
                      }}
                      name="envelope-square"
                    />
                  </Card>
                  email :
                </Text>
                <Text style={{left: 70, fontSize: 16, fontWeight: 'bold'}}>
                  {this.state.user_data ? this.state.user_data.email : null}
                </Text>

                <Text style={{left: 50, marginTop: 20}}>
                  <Card style={{width: 30, height: 30, borderRadius: 20}}>
                    <Icons
                      style={{fontSize: 20, color: '#07CC53', margin: 5}}
                      name="address-card"
                    />
                  </Card>
                  address :
                </Text>
                <Text style={{left: 70, fontSize: 16, fontWeight: 'bold'}}>
                  {this.state.user_data ? this.state.user_data.address : null}
                </Text>

                <Text style={{left: 50, marginTop: 20}}>
                  <Card style={{width: 30, height: 30, borderRadius: 20}}>
                    <Icons
                      style={{
                        fontSize: 20,
                        color: '#FD55BA',
                        left: 1,
                        margin: 5,
                      }}
                      name="phone-square"
                    />
                  </Card>
                  phone_number
                </Text>
                <Text style={{left: 70, fontSize: 16, fontWeight: 'bold'}}>
                  {this.state.user_data
                    ? this.state.user_data.phone_number
                    : null}
                </Text>
              </View>
            : <View style={{marginTop: 5}}>
                <Text style={{left: 50, marginTop: 20}}>
                  <Card style={{width: 30, height: 30, borderRadius: 20}}>
                    <Icons
                      style={{
                        fontSize: 20,
                        color: '#8D94F9',
                        left: 5,
                        margin: 3,
                      }}
                      name="user"
                    />
                  </Card>
                  Name
                </Text>
                <View style={styles.ContainerTextInput}>
                  <TextInput
                    style={{
                      width: 150,
                      left: 110,
                    }}
                    placeholder="ชื่อ"
                    maxLength={30}
                    underlineColorAndroid="#C5C5C5"
                    type="text"
                    color="black"
                    // onChangeText={}
                  />
                  <TextInput
                    style={{width: 150, left: 110}}
                    placeholder="นามสกุล"
                    maxLength={30}
                    underlineColorAndroid="#C5C5C5"
                    type="text"
                    color="black"
                    // onChangeText={}
                  />
                </View>
                
                <Text style={{left: 50, marginTop: 20}}>
                  <Card style={{width: 30, height: 30, borderRadius: 20}}>
                    <Icons
                      style={{
                        fontSize: 20,
                        color: '#E5D402',
                        left: 5,
                        left: 1,
                        margin: 5,
                      }}
                      name="envelope-square"
                    />
                  </Card>
                  email
                </Text>
                <Text style={{left: 70, fontSize: 16, fontWeight: 'bold'}}>
                  {this.state.user_data ? this.state.user_data.email : null}
                </Text>
                <Text style={{left: 50, marginTop: 20}}>
                  <Card style={{width: 30, height: 30, borderRadius: 20}}>
                    <Icons
                      style={{fontSize: 20, color: '#07CC53', margin: 5}}
                      name="address-card"
                    />
                  </Card>
                  address
                </Text>
                <Text style={{left: 70, fontSize: 16, fontWeight: 'bold'}}>
                  {this.state.user_data ? this.state.user_data.address : null}
                </Text>

                <Text style={{left: 50, marginTop: 20}}>
                  <Card style={{width: 30, height: 30, borderRadius: 20}}>
                    <Icons
                      style={{
                        fontSize: 20,
                        color: '#FD55BA',
                        left: 1,
                        margin: 5,
                      }}
                      name="phone-square"
                    />
                  </Card>
                  phone_number
                </Text>
                <Text style={{left: 70, fontSize: 16, fontWeight: 'bold'}}>
                  {this.state.user_data
                    ? this.state.user_data.phone_number
                    : null}
                </Text>
                <TouchableOpacity 
                 onPress={() => this.On_editSave ()}
                style={styles.buttonSave}>
                  <Text style={styles.buttonTextSave}>save</Text>
                </TouchableOpacity>
              </View>}
        </View>
      </ScrollView>
    );
  }
}
export default UserInfomation;
const styles = StyleSheet.create ({
  ContainerTextInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 40,
    marginTop: 10,
    width: 300,
    bottom: 20,
    right: 50,
  },
  buttonSave: {
    backgroundColor: '#2866CD',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 100,
    justifyContent: 'center',
    left: 160,
    bottom: 30,
  },
  buttonTextSave: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 18,
  },
});
