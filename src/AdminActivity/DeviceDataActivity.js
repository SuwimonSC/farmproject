import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Animated,
  Image,
  ScrollView,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Button, Card, DataTable, shadow, Switch,TextInput} from 'react-native-paper';
import Icons from 'react-native-vector-icons/FontAwesome';
import {CardItem, Header} from 'native-base';
import Modal from 'react-native-modal';
import {get, post, Delete, put} from '../Support/service';
import AsyncStorage from '@react-native-community/async-storage';
import { normalizeUnits } from 'moment';

class DeviceDataActivity extends Component {
  constructor (props) {
    super (props);
    this.state = {
      device_type: null,
      device_array: [],
      isVisible: false,
      data_device: null,
      device_del: null,
      h_focus: false,
      isVisible2:false,
    };
  }
  componentDidMount () {
    // console.log(this.props.route.params)
    let data = this.props.route.params.params.data;
    this.setState (
      {
        device_type: data,
      },
      () => {
        this.get_device ();
        // console.log ('Device : ', this.state.device_data);
      }
    );
  }

  get_device = () => {
    let type = this.state.device_type;
    // console.log ("type : ",type);
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        get (
          `api/v1/device/devices_data_admin/${type}`,
          user_token
        ).then (res => {
          if (res.success) {
            // console.log ('res', res.result);
            this.setState (
              {
                device_array: res.result,
              },
              () => {
                // console.log (' : ', this.state.);
              }
            );
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  _onOpenModal = element => {
    this.setState (
      {
        isVisible: true,
        data_device: element,
      },
      () => {
        console.log (element);
      }
    );
  };

  _onCloseModal = () => {
    this.setState ({
      isVisible: false,
    });
  };
  _onOpenModal_Add_device = () => {
    this.setState (
      {
        isVisible2: true,
      }, ); 
  };

  _onCloseModal_Add_device = () => {
    this.setState ({
      isVisible2: false,
    });
  };

  onDelete = (element, index) => {
    // console.log ('element :', element);
    Alert.alert (
      'คุณต้องการจะลบหรือไม่ ?',
      'กด ตกลง เพื่อลบ กด Clear เพื่อล้างข้อมูล',
      [
        {
          text: 'ยกเลิก',
          onPress: () => console.log ('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'clear',
          onPress: () => {
            this.Clear_device (element);
          },
        },
        {
          text: 'ตกลง',
          onPress: () => {
            this.Delete_device (index);
          },
        },
      ],
      {cancelable: false}
    );
  };

  Clear_device = element => {
    console.log ('element :', element);
    let obj = element;
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        put (
          obj,
          'api/v1/device/devices_data_clear_admin',
          user_token
        ).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  Delete_device = index => {
    // console.log ('index :', index);
    let data = this.state.device_array;
    let data_del = data[index];
    data.splice (index, 1);
    this.setState (
      {
        device_array: data,
        device_del: data_del,
      },
      () => {
        this.Delete_device_database ();
      }
    );
  };

  Delete_device_database = () => {
    let obj = this.state.device_del;
    // console.log (obj);
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        Delete (
          obj,
          'api/v1/device/devices_data_admin',
          user_token
        ).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };
  handleChange = (e, name) => {
    // console.log (e);
    this.setState ({
      [name]: e,
    });
  };
  AddDeice = async () => {
    let obj = {
      device_type: this.state.device_type,
      client_id:this.state.client_id
    };
    // this.props.navigation.navigate ('DeviceData',{screen:'DeviceData',params:{data : this.state.device_type}})
        // console.log ('OBJ : ', obj);
    try {
      await post (obj, `api/v1/device/devices_data_admin`, null).then (res => {
          if (res.success) {
            alert (res.message)
            this.get_device()
            this._onCloseModal_Add_device ();
          } else {
            alert (res.error_message);
          }
        });
    
    } catch (error) {
      alert (error);
    }
    
  };
  render () {
    return (
      <ScrollView>
        <View>
          <Header style={{backgroundColor: '#DDEAFF'}}>
            <Text style={{marginTop: 15, fontSize: 18}}>
              {this.state.device_type}
            </Text>
          </Header>
          <Icons
            style={{
              fontSize: 25,
              color: '#2866CD',
              bottom: 40,
              left: 20,
              justifyContent: 'space-between',
              width: 30,
            }}
            name="chevron-left"
            onPress={() => this.props.navigation.navigate ('Admin')}
          />
          <View>
            <Icons
              style={{fontSize: 25, color: '#2866CD', bottom: 65, left: 350}}
              name="plus"
              onPress={() => {
                      this._onOpenModal_Add_device ();
                    }}
            />
          </View>
          <DataTable style={{bottom: 53}}>
            <DataTable.Header style={{borderColor: '#2866CD', borderWidth: 1}}>
              <DataTable.Title style={{flex: 3, justifyContent: 'center'}}>
                Client_id
              </DataTable.Title>
              <DataTable.Title style={{flex: 9, justifyContent: 'center'}} />
            </DataTable.Header>
            {this.state.device_array.map ((element, index) => {
              return (
                <DataTable.Row
                  style={element.user_id ? styles.br_notnull : styles.br_null}
                >
                  <DataTable.Cell style={{borderWidth: 0, flex: 6}}>
                    {element.client_id}
                  </DataTable.Cell>

                  <DataTable.Cell
                    style={styles.btn_de_edit_del}
                    onPress={() => {
                      this._onOpenModal (element);
                    }}
                  >
                    <Icons name="search" style={{fontSize: 20}} />
                  </DataTable.Cell>

                  <DataTable.Cell
                    style={styles.btn_de_edit_del}
                    onPress={() =>
                      this.props.navigation.navigate ('EditDevice', {
                        screen: 'EditDevice',
                        params: {data: element},
                      })}
                  >
                    <Icons name="pencil" style={{fontSize: 20}} />
                  </DataTable.Cell>

                  <DataTable.Cell
                    style={styles.btn_del}
                    onPress={() => this.onDelete (element, index)}
                  >
                    <Icons
                      name="trash"
                      style={{fontSize: 20, color: '#E90000'}}
                    />
                  </DataTable.Cell>

                </DataTable.Row>
              );
            })}
          </DataTable>
        </View>
        <Modal
          isVisible={this.state.isVisible}
          style={{
            backgroundColor: 'white',
            borderRadius: 20,
          }}
        >
          <Icons
            style={{
              fontSize: 25,
              left: 330,
              bottom: 160,
              color: 'black',
            }}
            onPress={this._onCloseModal}
            name="times-circle"
          />

          {this.state.data_device
            ? <View style={styles.data}>
                <Text style={{margin: 5, fontSize: 17}}>
                  Client_id :
                  {' '}
                  {this.state.data_device
                    ? this.state.data_device.client_id
                    : '-ไม่มี-'}

                </Text>
                <Text style={{margin: 5, fontSize: 17}}>
                  device_id :
                  {' '}
                  {this.state.data_device
                    ? this.state.data_device.device_id
                    : '-ไม่มี-'}

                </Text>
                <Text style={{margin: 5, fontSize: 17}}>
                  device_name :
                  {' '}
                  {this.state.data_device
                    ? this.state.data_device.device_name
                    : '-ไม่มี-'}

                </Text>

                <Text style={{margin: 5, fontSize: 17}}>
                  group_id :
                  {' '}
                  {this.state.data_device.group_id != null
                    ? this.state.data_device.group_id
                    : 'ไม่มี'}

                </Text>
                <Text style={{margin: 5, fontSize: 17}}>
                  user_id ที่กำลังใช้งาน :
                  {' '}
                  {this.state.data_device.user_id != null
                    ? this.state.data_device.user_id
                    : 'ไม่มี'}

                </Text>
              </View>
            : null}
        </Modal>
        <Modal 
         isVisible={this.state.isVisible2}
          style={{
            backgroundColor: 'white',
            borderRadius: 20,
           }}>          
           <Icons
              style={{
                fontSize: 25,
                left:330,
                bottom:160,
                color: '#2866CD',
              }}
              onPress={this._onCloseModal_Add_device}
              name="times-circle"
            />
            <Text style={{fontSize:18,bottom:0,textAlign:'center'}}>{this.state.device_type ?this.state.device_type:null}</Text>
      
          <TextInput
            style={{width: 300, left: 40,height:50,bottom:0}}
            maxLength={30}
            mode="outlined"
            underlineColor="#2C8D5D"
            theme={{ colors: { placeholder: 'grey', background: '#DDEAFF', text: 'black', primary: '#2866CD' }}}
            type="text"
            label="Client ID"
            onChangeText={e => this.handleChange (e, 'client_id')}
          />         
            <TouchableOpacity
              style={styles.buttonSave}
              onPress={() => {
                this.AddDeice ();
              }} >          
              <Text style={styles.buttonTextSave}>save</Text>
            </TouchableOpacity>      
        </Modal>
      </ScrollView>
    );
  }
}
export default DeviceDataActivity;

const styles = StyleSheet.create ({
  ContainerTextInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 40,
    marginTop: 10,
    width: 300,
    bottom: 20,
    right: 50,
  },
  cardStyle: {
    width: 370,
    marginBottom: 5,
    backgroundColor: '#52BA78',
    borderRadius: 10,
    left: 20,
  },
  data: {
    width: 300,
    opacity: 1,
    marginLeft: 30,
    paddingVertical: 50,
    borderWidth: 0.5,
    borderColor: '#20232a',
    borderRadius: 30,
    backgroundColor: '#DDEAFF',
    color: '#20232a',
    textAlign: 'center',
    fontWeight: 'bold',
    padding: 20,
    shadowColor: '#f8f8f8',
    left: 8,
    bottom: 70,
  },
  btn_de_edit_del: {
    borderRightWidth: 2,
    borderColor: '#9c9c9c',
    flex: 2,
    justifyContent: 'center',
  },

  btn_del: {
    color: '#ff2146',
    flex: 2,
    justifyContent: 'center',
  },
  br_notnull: {
    borderBottomWidth: 0,
    marginBottom: 10,
    marginTop: 5,
    borderLeftWidth: 10,
    borderColor: '#2866CD',
    backgroundColor: '#fff',
    margin: 5,
  },
  br_null: {
    borderBottomWidth: 0,
    marginBottom: 10,
    marginTop: 5,
    borderLeftWidth: 10,
    borderColor: '#e01f33',
    backgroundColor: '#fff',
    margin: 5,
  },
  buttonSave: {
    backgroundColor: '#2866CD',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 100,
    justifyContent: 'center',
    left: 150,
    bottom:0
  },
  buttonTextSave: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 15,
  },
});
