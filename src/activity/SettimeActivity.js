import React, {Component} from 'react';
import Icons from 'react-native-vector-icons/FontAwesome';
import {
  View,
  Text,
  Alert,
  Image,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Modal from 'react-native-modal';
import moment from 'moment';
import {DataTable, Switch} from 'react-native-paper';
import TimePicker from 'react-native-24h-timepicker';
import AsyncStorage from '@react-native-community/async-storage';
import {get, post, Delete, put} from '../Support/service';
import io from 'socket.io-client';

var socket_client = io ('http://192.168.1.96:3434');
// var socket_client = io ('http://192.168.0.57:3434');

class SettimeActivity extends Component {
  constructor (props) {
    super (props);
    this.state = {
      data_device: null,
      data_group: null,
      isVisible: false,
      date: moment ().format ('YYYY-MM-DD'),
      timeON: moment ().format ('HH:mm'),
      timeOFF: moment ().format ('HH:mm'),
      open: false,
      data_to_database: null,
      data_time: [],
    };
  }

  componentDidMount () {
    console.log (this.props.route.params.params);
    this.setState (
      {
        data_device: this.props.route.params.params.data_device,
        data_group: this.props.route.params.params.data_group,
      },
      () => {
        this.get_time ();
      }
    );
  }

  _onOpenModal = () => {
    this.setState ({
      isVisible: true,
    });
  };
  _onCloseModal = () => {
    this.setState ({
      isVisible: false,
    });
  };
  onTimeONCancel () {
    this.TimePicker.close ();
  }

  onTimeONConfirm (hour, minute) {
    this.setState (
      {
        timeON: `${hour}:${minute}`,
      },
      () => {}
    );
    this.TimePicker.close ();
  }

  onTimeOFFCancel () {
    this.TimePicker.close ();
  }

  onTimeOFFConfirm (hour, minute) {
    this.setState (
      {
        timeOFF: `${hour}:${minute}`,
      },
      () => {}
    );
    this.TimePicker.close ();
  }
  get_time = async () => {
    let id = this.state.data_device.client_id;
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        get (`api/v1/device/time_to_app/${id}`, user_token).then (res => {
          if (res.success) {
            // console.log ('res', res.result);
            this.setState (
              {
                data_time: res.result,
              },
              () => {
                // console.log (' : ', this.state.);
              }
            );
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  _SetOn = () => {
    this.TimePicker.open ();
    this.setState ({
      open: true,
    });
  };

  _SetOff = () => {
    this.TimePicker.open ();
    this.setState ({
      open: false,
    });
  };

  onSave_time = () => {
    let obj = {
      client_id: this.state.data_device.client_id,
      device_time: [
        {
          time: moment (`${this.state.date} ${this.state.timeON}`).format (
            'YYYY-MM-DD HH:mm:ss'
          ),
          action: 'on',
        },
        {
          time: moment (`${this.state.date} ${this.state.timeOFF}`).format (
            'YYYY-MM-DD HH:mm:ss'
          ),
          action: 'off',
        },
      ],
    };

    this.setState (
      {
        data_to_database: obj,
      },
      () => {
        this.onSave_to_database ();
      }
    );
    // console.log("obj : ",obj)
  };

  onSave_to_database = async () => {
    console.log ('1');
    let object = this.state.data_to_database;

    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        post (object, 'api/v1/device/setup_time', user_token).then (res => {
          if (res.success) {
            this.get_time ();
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };
  clickON () {
    let payload = {
      device_name: 'solenoid_valve_',
      device_id: '201',
      status: '{"status":"on"}',
    };
    console.log (payload);
    socket_client.emit ('arduino', payload, () => {
      socket_client.close ();
    });
  }

  clickOFF () {
    let payload = {
      device_name: 'solenoid_valve_',
      device_id: '201',
      status: '{"status":"off"}',
    };
    console.log (payload);

    socket_client.emit ('arduino', payload, () => {
      socket_client.close ();
    });
  }

  render () {
    return (
      <View>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            left: 20,
          }}
        >
          <Icons
            style={{fontSize: 25, color: '#2C8D5D', marginTop: 40, width: 30}}
            name="chevron-left"
            onPress={() =>
              this.props.navigation.navigate ('Group', {
                screen: 'Group',
                params: {data: this.state.data_group},
              })}
          />
          <Icons
            style={{
              fontSize: 25,
              color: '#2C8D5D',
              marginTop: 40,
              width: 30,
              right: 40,
            }}
            name="plus"
            onPress={() => {
              this._onOpenModal ();
            }}
          />
        </View>
        <Modal
          isVisible={this.state.isVisible}
          style={{
            backgroundColor: 'white',
            borderRadius: 20,
          }}
        >
          <Icons
            style={{
              fontSize: 25,
              left: 330,
              bottom: 280,
              color: '#2C8D5D',
            }}
            onPress={() => {
              this._onCloseModal ();
            }}
            name="times-circle"
          />

        </Modal>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 100,
          }}
        >
          <TouchableOpacity
            style={{
              backgroundColor: 'green',
              marginTop: 5,
              width: 70,
              height: 30,
              left: 50,
              bottom: 80,
              borderRadius: 10,
            }}
            onPress={() => {
              this.clickON ();
            }}
          >
            <Text style={{left: 20, marginTop: 2, color: 'white'}}>ON</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: 'red',
              bottom: 80,
              width: 70,
              height: 30,
              right: 40,
              borderRadius: 10,
            }}
            onPress={() => {
              this.clickOFF ();
            }}
          >
            <Text style={{left: 20, color: 'white', marginTop: 2}}>OFF</Text>
          </TouchableOpacity>

        </View>
        <DataTable style={{bottom: 20}}>

          <DataTable.Header style={{borderColor: 'green', borderWidth: 1}}>
            <DataTable.Title>เวลาปิด</DataTable.Title>
            <DataTable.Title>เวลาเปิด</DataTable.Title>
            <DataTable.Title />
          </DataTable.Header>

          {this.state.data_time.map ((element, index) => {
            return (
              <ScrollView>

                <DataTable.Row style={styles.DataTableRowStyle}>
                  <DataTable.Cell>
                    {moment (element.time_array[0].time).format ('HH:mm')}
                  </DataTable.Cell>
                  <DataTable.Cell>
                    {moment (element.time_array[1].time).format ('HH:mm')}
                  </DataTable.Cell>
                  <DataTable.Cell>
                    <Text>{element.action}</Text>
                    {/* <Switch
                      style={{left: 150, scaleY: 1, scaleX: 1}}
                      trackColor={{false: '#767577', true: '#81b0ff'}}
                      onValueChange={switchValue => {
                        let data_time = this.state.data_time;
                        data_time[index].action = switchValue ? 'on' : 'off';

                        this.setState ({data_time: data_time});
                      }}
                      value={element.action == 'on' ? true : false}
                    /> */}
                  </DataTable.Cell>

                </DataTable.Row>

              </ScrollView>
            );
          })}

        </DataTable>
        <Modal
          isVisible={this.state.isVisible}
          style={{backgroundColor: 'white', borderRadius: 20}}
        >
          <Icons
            style={{
              fontSize: 25,
              left: 330,
              bottom: 20,
              color: 'black',
            }}
            onPress={this._onCloseModal}
            name="times-circle"
          />
          <View>
            <View style={{bottom: 50}}>
              <TouchableOpacity onPress={this._SetOn} style={styles.buttonOpen}>
                <Text style={styles.buttonText}>ตั้งเวลาเปิด</Text>

              </TouchableOpacity>

              <TouchableOpacity onPress={this._SetOff} style={styles.buttonOff}>
                <Text style={styles.buttonText}>ตั้งเวลาปิด</Text>

              </TouchableOpacity>

              <Text style={styles.textOpen}>{this.state.timeON}</Text>

              <Text style={styles.textOff}>{this.state.timeOFF}</Text>

              <TouchableOpacity
                onPress={() => this.onSave_time (this._onCloseModal ())}
                style={styles.buttonSave}
              >
                <Text style={styles.buttonTextSave}>save</Text>
              </TouchableOpacity>

            </View>
          </View>
          {this.state.open
            ? <View>
                <TimePicker
                  ref={ref => {
                    this.TimePicker = ref;
                  }}
                  onCancel={() => this.onTimeONCancel ()}
                  onConfirm={(hour, minute) =>
                    this.onTimeONConfirm (hour, minute)}
                />
              </View>
            : <View>
                <TimePicker
                  ref={ref => {
                    this.TimePicker = ref;
                  }}
                  onCancel={() => this.onTimeOFFCancel ()}
                  onConfirm={(hour, minute) =>
                    this.onTimeOFFConfirm (hour, minute)}
                />
              </View>}
        </Modal>
      </View>
    );
  }
}
export default SettimeActivity;

const styles = StyleSheet.create ({
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    bottom: 50,
  },
  DataTableRowStyle: {
    backgroundColor: '#DAF7A6',
    paddingLeft: 15,
    paddingRight: 15,
    borderColor: 'green',
    borderWidth: 1,
  },
  buttonOpen: {
    backgroundColor: '#4EB151',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 150,
    left: 30,
  },
  buttonOff: {
    backgroundColor: 'red',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 150,
    left: 200,
    bottom: 145,
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 20,
  },
  buttonSave: {
    backgroundColor: 'blue',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 100,
    justifyContent: 'center',
    left: 140,
    marginTop: 80,
  },
  buttonTextSave: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 18,
  },
  textOpen: {
    fontSize: 20,
    marginTop: 10,
    left: 70,
    bottom: 180,
  },
  textOff: {
    fontSize: 20,
    marginTop: 10,
    left: 240,
    bottom: 217,
  },
});
