import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {LoginButton} from '../Components/LoginButton';
import FacebookButton from '../Components/FacebookButton';
import Icons from 'react-native-vector-icons/FontAwesome';
import {ip} from '../Support/contance';
import {get, post} from '../Support/service';
import RButton from '../Components/RegisterButton';
// import LoginButton from '../Components/FacebookButton';
import {AccessToken} from 'react-native-fbsdk';
import {LoginManager} from 'react-native-fbsdk';
import AsyncStorage from '@react-native-community/async-storage';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  Animated,
  Image,
} from 'react-native';
import {GraphRequest, GraphRequestManager} from 'react-native-fbsdk';
// const user_token = AsyncStorage.getItem('user_token')

var jwt_decode = require ('jwt-decode');

const Purple = '#CE26F8';

class LoginActivity extends Component {
  constructor (props) {
    super (props);
    this.state = {
      email: '',
      password: '',
      icon_eye: 'eye-slash',
      showPassword: true,
      user_name: '',
      avatar_url: '',
      avatar_show: false,
    };
  }

  componentDidMount () {
    this._loadInitalState ().done ();
  }

  handleChange = (e, name) => {
    this.setState ({
      [name]: e,
    });
  };

  user_login = async () => {
    let body = {
      email: this.state.email,
      password: this.state.password,
    };
    console.log ('body : ', body);
    let decode = null;
    try {
      await post (body, 'api/v1/user/user_login', null).then (res => {
        if (res.success) {
          // alert(res.token)
          AsyncStorage.setItem ('user_token', res.token);
          decode = jwt_decode (res.token);
          
          if (decode.type === 0) {
            this.props.navigation.navigate('Admin')
          }
          if (decode.type === 1) {
            this.props.navigation.navigate ('Main');
          }
          // window.location.href = '/';
          // console.log ('Signin : ', res.token);
        } else {
          alert (res.error_message);
        }
      });
    } catch (error) {
      alert (error);
    }
  };

  _loadInitalState = async () => {
    var value = await AsyncStorage.getItem ('user');
    if (value !== null) {
      this.props.navigation.navigate ('Home');
    }
  };

  _OnChangeShowPassword = () => {
    let newState;
    if (this.state.showPassword) {
      newState = {
        icon_eye: 'eye',
        showPassword: false,
      };
    } else {
      newState = {
        icon_eye: 'eye-slash',
        showPassword: true,
      };
    }
    this.setState (newState);
  };

  _facebookLogin = async () => {
    // var result = await LoginManager.logInWithPermissions(['public_profile'])
    await LoginManager.logInWithPermissions (['public_profile']).then (
      function (result) {
        if (result.isCancelled) {
          console.log ('Login cancelled');
        } else {
          console.log (
            'Login success with permissions: ' +
              result.grantedPermissions.toString ()
          );
        }
      },
      function (error) {
        console.log ('Login fail with error: ' + error);
      }
    );
  };
  get_Response_Info = (error, result) => {
    if (error) {
      Alert.alert ('Error fetching data: ' + error.toString ());
    } else {
      this.setState ({user_name: 'Welcome' + ' ' + result.name});

      this.setState ({avatar_url: result.picture.data.url});

      this.setState ({avatar_show: true});

      console.log (result);
    }
  };
  onLogout = () => {
    this.setState ({user_name: null, avatar_url: null, avatar_show: false});
  };

  render () {
    return (
      <View style={styles.Viewcontainer}>
        <ImageBackground
          style={styles.Viewcontainer}
          source={require ('../images/bggreen.png')}
        >

          <View style={styles.Container}>
            <View style={{marginTop: 100}}>
              <Text style={styles.TextLabel}>USERNAME</Text>
            </View>
            <View style={styles.ContainerTextInput}>
              {/* <Image
                source={require ('../images/email.png')}
                style={styles.iconInput}
              /> */}
              <TextInput
                style={styles.TextInput}
                placeholderTextColor="#A6A6A6"
                keyboardType="email-address"
                placeholder={'email here'}
                selectionColor="white"
                color="white"
                maxLength={30}
                underlineColorAndroid="transparent"
                id="email"
                onChangeText={e => this.handleChange (e, 'email')}
              />
            </View>
            <View style={{marginBottom: 10}} />
            <Text style={styles.TextLabel}>PASSWORD</Text>
            <View style={styles.ContainerTextInput}>

              {/* <Image
                source={require ('../images/password.png')}
                style={styles.iconInput}
              /> */}

              <TextInput
                style={styles.TextInput}
                secureTextEntry={this.state.showPassword}
                placeholderTextColor="#A6A6A6"
                placeholder={'password here'}
                selectionColor="white"
                color="white"
                maxLength={20}
                underlineColorAndroid="transparent"
                id="password"
                onChangeText={e => this.handleChange (e, 'password')}
              />
              <Icons
                style={styles.IconCheck}
                name={this.state.icon_eye}
                color="#837F88"
                size={20}
                onPress={() => {
                  this._OnChangeShowPassword ();
                }}
              />
            </View>
            <View>
              <LoginButton
                style={styles.bottonLogin}
                title="Login"
                color="#E75878"
                onPress={() => {
                  this.user_login ();
                }}
              />
            </View>
            <View
              style={{
                backgroundColor: 'white',
                width: 300,
                height: 3,
                borderRadius: 1,
                margin: 30,
              }}
            />
            <View />
            <View style={{marginBottom: 20}}>
              <Text style={{color: 'white'}}>or Login with social network</Text>
            </View>

            <View>
              <RButton
                style={styles.bottonRegister}
                title="Register"
                color="#E12E23"
                onPress={() => {
                  this.props.navigation.navigate ('Register');
                }}
              />
            </View>
            <View>
              {this.state.avatar_url
                ? <Image
                    source={{uri: this.state.avatar_url}}
                    style={styles.imageStyle}
                  />
                : null}

              <Text style={styles.text}> {this.state.user_name} </Text>

              <FacebookButton
                style={styles.bottonRegister}
                title="Continue with facebook"
                color="#E75878"
                onPress={() => {
                  this._facebookLogin ();
                }}
                readPermissions={['public_profile']}
                publishPermissions={['email']}
                onLoginFinished={(error, result) => {
                  if (error) {
                    alert ('Login failed with error: ' + error.message);
                  } else if (result.isCancelled) {
                    alert ('Login was cancelled');
                  } else {
                    AccessToken.getCurrentAccessToken ().then (data => {
                      console.log (data.accessToken.toString ());
                      const processRequest = new GraphRequest (
                        '/me?fields=name,picture.type(large)',
                        null,
                        this.get_Response_Info
                      );
                      // Start the graph request.
                      new GraphRequestManager ()
                        .addRequest (processRequest)
                        .start ();
                    });
                  }
                }}
                onLogoutFinished={() => alert (this.onLogout)}
              />

            </View>
          </View>

        </ImageBackground>

      </View>
    );
  }
}

const styles = StyleSheet.create ({
  Container: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    marginTop: 10,
    color: 'red',
  },
  ContainerTextInput: {
    flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'black',
    height: 45,
    borderRadius: 10,
    margin: 10,
    width: 300,
  },
  Viewcontainer: {
    flex: 1,
    justifyContent: 'center',
  },
  bottonLogin: {
    justifyContent: 'center',
    width: 300,
  },
  bottonRegister: {
    justifyContent: 'center',

    width: 300,
  },
  imageTextInput: {
    height: 30,
    width: 30,
  },
  iconInput: {
    padding: 10,
    marginTop: 10,
    height: 20,
    width: 20,
    resizeMode: 'stretch',
    alignItems: 'center',
    resizeMode: 'stretch',
    left: 10,
  },
  TextInput: {
    height: 40,
    marginTop: 2,
    width: 300,
    borderRadius: 20,
    fontSize: 14,
    left: 15,
    flex: 1,
  },
  IconCheck: {
    marginTop: 12,
    right: 20,
  },
  TextLabel: {
    right: 100,
    color: 'white',
    borderEndWidth: 20,
    fontWeight: 'bold',
    fontSize: 12,
  },
  text: {
    fontSize: 20,
    color: '#000',
    textAlign: 'center',
    padding: 20,
  },

  imageStyle: {
    width: 200,
    height: 300,
    resizeMode: 'contain',
  },
});
export default LoginActivity;
