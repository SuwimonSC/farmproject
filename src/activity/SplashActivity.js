import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  Animated,
  Image,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

var jwt_decode = require ('jwt-decode');

class SplashActivity extends Component {
  componentDidMount () {
    let decode = null
    setTimeout (() => {
      AsyncStorage.getItem ('user_token').then (user_token => {
        decode = jwt_decode (user_token)
        console.log ('decode', decode.type);
        if(decode.type === 0){
          console.log ('Admin');
          this.props.navigation.navigate('Admin')
        }
        else if(decode.type ===  1){
          console.log ('Main');
          this.props.navigation.navigate('Main')
        }
        else{
          console.log ('Login');
          this.props.navigation.navigate ('Login');
        }
        // this.props.navigation.navigate ('Login');
      });
    },1500);
  }

  render () {
    return (
      <View>
        <ImageBackground
          style={{width: 415, height: 660}}
          source={require ('../images/splash-screen.png')}
        />
      </View>
    );
  }
}
export default SplashActivity;
