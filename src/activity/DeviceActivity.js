import React, {Component} from 'react';
import io from 'socket.io-client';
import {
  View,
  Text,
  Alert,
  Image,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
 
} from 'react-native';
import {TextInput} from 'react-native-paper'
import {get, post, Delete, put} from '../Support/service';
import AsyncStorage from '@react-native-community/async-storage';
import Icons from 'react-native-vector-icons/FontAwesome';

var socket_client = io ('http://192.168.0.57:3434');

class DeviceActivity extends Component {
  constructor (props) {
    super (props);
    this.state = {
      device_name: '',
      client_id: '',
      data_group: null,
    };
  }

  componentDidMount () {
    let data = this.props.route.params.params.data;
    this.setState (
      {
        data_group: data,
      },
      () => {
        // console.log ('data_group', this.state.data_group);
      }
    );
  }

  handleChange = (e, name) => {
    // console.log (e);
    this.setState ({
      [name]: e,
    });
  };

  AddDeice = async () => {
    let obj = {
      device_name: this.state.device_name,
      client_id: this.state.client_id,
      group_id: this.state.data_group.group_id,
    };

    console.log ('OBJ : ', obj);

    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        post (obj, 'api/v1/device/add_device', user_token).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  render () {
    return (
      <SafeAreaView style={{backgroundColor:'white',height:1000}}>
        <View>
         <View>
          <Icons
            style={{fontSize: 25, margin: 20, color: '#2C8D5D'}}
            name="chevron-left"
            onPress={() => this.props.navigation.navigate ('Group',this.state.data_group)}
          />
        </View>
          <TextInput
            style={{width: 300, left: 60, marginTop: 30,height:50,}}
            maxLength={30}
            mode="outlined"
            underlineColor="#2C8D5D"
            theme={{ colors: { placeholder: 'grey', background: '#f4fcf2', text: 'black', primary: '#2C8D5D' }}}
            type="text"
            label="Device Name"
            onChangeText={e => this.handleChange (e, 'device_name')}
          />
          <TextInput
            style={{width: 300, left: 60, marginTop:10,height:50}}
            placeholderTextColor="white"
            maxLength={30}
            theme={{ colors: { placeholder: 'grey', background: '#f4fcf2', text: 'black', primary: '#2C8D5D' }}}
            underlineColor="#2C8D5D"
            type="text"
            color="white"
            label="Client ID"
            mode='outlined'
            onChangeText={e => this.handleChange (e, 'client_id')}
          />
          <View>
            <TouchableOpacity
              style={styles.buttonSave}
              onPress={() => {
                this.AddDeice ();
                this.props.navigation.navigate ('Group');
              }}
            >
              <Text style={styles.buttonTextSave}>save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
export default DeviceActivity;

const styles = StyleSheet.create ({
  buttonSave: {
    backgroundColor: '#2C8D5D',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 100,
    justifyContent: 'center',
    left: 170,
  },
  buttonTextSave: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 18,
  },
});
