import 'react-native-gesture-handler';
import React, {Component} from 'react';
import Icons from 'react-native-vector-icons/FontAwesome';
import {
  Image,
  StyleSheet,
  AsyncStorage,
  ImageBackground,
  StatusBar,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Left,
  Body,
  Right,
  View,
} from 'native-base';
import socketIOClient from 'socket.io-client';
import io from 'socket.io-client';

// var socket_client = io ('http://172.20.10.3:3434');

const user_token = AsyncStorage.getItem ('user_token');

const Screens = [
  { bg: 'cornfield.jpg'},
  {iconName: 'circle', temp: 23, bg: 'creative-rain_01.jpg'},
  {iconName: 'circle', temp: 27, bg: 'sunny-day.jpg'},
];
const ShowScreen = Screens[0];
StatusBar.setBarStyle ('light-content');
StatusBar.setBackgroundColor ('black');       

class MainActivity extends Component {
  constructor () {
    super ();
    this.state = {
      showtemp: '',
      endpoint: 'http://192.168.0.57:3434',
      message: [],
      temp: '',
      humid: '',
      ec:'',
      ph:'',
      w_temp:''
    };
  }
  componentDidMount () {
    this.get_socket ();
    // this.response ();
    this.get_ec_ph();
  }

  get_socket = () => {
    const {endpoint, message} = this.state;
    const temp = message;
    const socket = socketIOClient (endpoint);
    socket.on ('aom', messageNew => {
      // console.log ('TemperatureActivity massage:', messageNew);
      this.setState ({
        temp: messageNew.temp,
        humid: messageNew.humid,
      });

      temp.push (messageNew);
      this.setState ({message: temp});
    });
  };

  get_ec_ph = (client) => {
   
    const {endpoint, message} = this.state;
    const ph = message; 
    const socket = socketIOClient (endpoint);
    socket.on ('EC_PH_value', messageNew => {
      console.log ('massage:', messageNew);

      let value = messageNew.payload

      switch(messageNew.from){
        case "ec_sensor_2":
          this.setState ({
            // ec: messageNew.ec,
            w_temp:value.temp,
            ec:value.ec,
          });
        break;
        case "ph_sensor_2":
          this.setState ({
            ph:value.ph
          });
        break;
      }
    
      // temp.push (messageNew);
      // this.setState ({message: temp});
    });
  };
  render () {
    return (
      <Container>
            <CardItem cardBody>
              <ImageBackground 
                // style={styles.bgStyle}
                source={require('../images/bggreen.png')}                
                style={styles.background}>
                <Icons style={styles.IconStyle} name={ShowScreen.iconName} />
                <Text style={styles.TempStyle}>
                  {ShowScreen.temp}
                </Text>
                <Text style={styles.textStyle}>
                  {ShowScreen.text}
                </Text>
                <View style={styles.temp_humid}>
                  <Text style={{left: 30,fontSize:40}}>
                  TEMP   {this.state.temp || 0}   °C
                  </Text>
                  <Text style={{left: 30,fontSize:40}}>
                   HUMID {this.state.humid || 0}   %
                  </Text>
                </View>
                <View style={styles.EC_PH}>   
                  <Text style={{left: 50,fontSize:40,color:'black'}}>
                    EC: {this.state.ec || 0} 
                  </Text>
                  <Text style={{left: 50,fontSize:40,color:'black'}}>
                   TW: {this.state.w_temp || 0}
                  </Text>
                  <Text style={{left: 50,fontSize:40}}>
                    PH: {this.state.ph || 0} 
                  </Text>
                </View>
              </ImageBackground>
            </CardItem>
      </Container>
    );
  }
}
const styles = StyleSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#D0D0D0',
  },
  background:{
    flex: 1,
    width:420,
    height:850,
   
  },
  bgStyle: {
    height: 100,
    width: 100,
    position: 'relative',
    top: 2,
    left: 2,
  },
  IconStyle: {
    left: 300,
    top: 70,
    color: 'white',
    fontSize: 30,
  },
  TempStyle: {
    fontWeight: 'bold',
    color: 'white',
    position: 'absolute',
    bottom: 350,
    left: 150,
    fontSize: 120,
  },
  textStyle: {
    fontWeight: 'bold',
    color: 'white',
    position: 'absolute',
    bottom: 305,
    left: 140,
    fontSize: 50,
  },
  temp_humid:{
    width:350,
    // opacity:0.5,
    marginTop:10,
    marginLeft:30,
    paddingVertical: 50,
    borderWidth: 1,
    borderColor: "#20232a",
    borderRadius: 30,
    backgroundColor: "#f0f8ff",
    color: "#20232a",
    textAlign: "center",
    fontWeight: "bold"
  },
  EC_PH:{
    width:350,
    // opacity:0.5,
    marginTop: 20,
    marginLeft:30,
    paddingVertical: 50,
    borderWidth: 1,
    borderColor: "#20232a",
    borderRadius: 30,
    backgroundColor: "#f0f8ff",
    color: "#20232a",
    textAlign: "center",
    fontWeight: "bold"
  },

});

export default MainActivity;