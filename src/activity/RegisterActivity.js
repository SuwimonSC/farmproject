import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {LoginButton} from '../Components/LoginButton';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert,
  TouchableOpacity
} from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome';
import io from 'socket.io-client';
import { post } from '../Support/service';

const Green = '#11FF00';

class RegisterActivity extends Component {
  constructor (props) {
    super (props);
    this.state = {
      firstname: '',
      lastName: '',
      email: '',
      phoneNumber: '',
      password: '',
      password_re: '',
      icon_eye: 'eye-slash',
      showPassword: true,
      icon_eye2: 'eye-slash',
      showPassword2: true,
      address: '',
      textpasswordcheck: '',
      check_pass: true,
      ButtonStateHolder : true ,
    };
  }

  componentDidMount () {
    this._loadInitalState ().done ();
  }

  _loadInitalState = async () => {
    var value = await AsyncStorage.getItem ('user');
    if (value !== null) {
      this.props.navigation.navigate ('Home');
    }
  };

  _OnChangeShowPassword = () => {
    let newState;
    if (this.state.showPassword) {
      newState = {
        icon_eye: 'eye',
        showPassword: false,
      };
    } else {
      newState = {
        icon_eye: 'eye-slash',
        showPassword: true,
      };
    }
    this.setState (newState);
  };

  _OnChangeShowPassword2 = () => {
    let newState;
    if (this.state.showPassword2) {
      newState = {
        icon_eye2: 'eye',
        showPassword2: false,
      };
    } else {
      newState = {
        icon_eye2: 'eye-slash',
        showPassword2: true,
      };
    }
    this.setState (newState);
  };

  handleChange = (e, name) => {
    console.log (e);
    this.setState (
      {
        [name]: e,
      },
      () => {
        if (name == 'password' || name == 'password_re') {
          this._CheckPassword ();
        }
      }
    );
  };

  _CheckPassword = () => {
    if (!this.state.password || this.state.password == this.state.password_re) {
      // console.log ('True');
      this.setState ({
        textpasswordcheck: 'Password Matching',
        check_pass: true,
      });
    } else {
      // console.log ('false');
      this.setState ({
        textpasswordcheck: 'Password is Wrong',
        check_pass: false,
      });
    }
  };

  _OnRegister = async () => {
    let body = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      email: this.state.email,
      phonenumber: this.state.phoneNumber,
      address: this.state.address,
      password: this.state.password,
    };
    // console.log (obj);
    try {
      await post (body, 'api/v1/user/user_register', null).then (res => {
        if (res.success) {
        } else {
          alert (res.error_message);
        }
      });
    } catch (error) {
      alert (error);
    }
    
  };
  render () {
    return (
      <View style={styles.Container}>
        <ImageBackground
          style={styles.ImageContainer}
          source={require ('../images/bggreen.png')}
        >
          <View style={{flex: 1, marginTop: 145, left: 55}}>
            <View style={styles.ContainerTextInput}>
              <Image
                source={require ('../images/user.png')}
                style={styles.iconInput}
              />
              <TextInput
                style={styles.TextInput}
                autoCapitalize="none"
                placeholder={'FirstName'}
                selectionColor={Green}
                underlineColorAndroid="transparent"
                maxLength={30}
                onChangeText={e => this.handleChange (e, 'firstname')}
              />
              <TextInput
                style={styles.TextInput}
                autoCapitalize="none"
                placeholder={'LastName'}
                selectionColor={Green}
                underlineColorAndroid="transparent"
                maxLength={30}
                onChangeText={e => this.handleChange (e, 'lastname')}
              />
            </View>
            <View style={styles.ContainerTextInput}>
              <Image
                source={require ('../images/email.png')}
                style={styles.iconInput}
              />
              <TextInput
                style={styles.TextInput}
                keyboardType="email-address"
                placeholder={'Email Address'}
                selectionColor={Green}
                underlineColorAndroid="transparent"
                maxLength={30}
                onChangeText={e => this.handleChange (e, 'email')}
              />

            </View>
            <View style={styles.ContainerTextInput}>
              <Image
                source={require ('../images/phone.png')}
                style={styles.iconInput}
              />
              <TextInput
                style={styles.TextInput}
                placeholder={'Phone Number'}
                selectionColor={Green}
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                maxLength={20}
                onChangeText={e => this.handleChange (e, 'phoneNumber')}
              />

            </View>
            <View style={styles.ContainerTextInput}>
              <Image
                source={require ('../images/pin.png')}
                style={styles.iconInput}
              />
              <TextInput
                style={styles.TextInput}
                placeholder={'Address'}
                selectionColor={Green}
                underlineColorAndroid="transparent"
                onChangeText={e => this.handleChange (e, 'address')}
              />

            </View>

            <View style={styles.ContainerTextInput}>
              <Image
                source={require ('../images/password.png')}
                style={styles.iconInput}
              />
              <TextInput
                style={styles.TextInput}
                placeholder={'Password'}
                secureTextEntry={this.state.showPassword}
                selectionColor={Green}
                underlineColorAndroid="transparent"
                onChangeText={e => this.handleChange (e, 'password')}
              />

              <Icons
                style={styles.PasswordCheck}
                name={this.state.icon_eye}
                color="#837F88"
                size={20}
                onPress={() => {
                  this._OnChangeShowPassword ();
                }}
              />

            </View>

            <View style={styles.ContainerTextInput}>
              <Image
                source={require ('../images/password.png')}
                style={styles.iconInput}
              />
              <TextInput
                style={styles.TextInput}
                placeholder={'Confirm password'}
                secureTextEntry={this.state.showPassword2}
                selectionColor={Green}
                maxLength={20}
                underlineColorAndroid="transparent"
                onChangeText={e => this.handleChange (e, 'password_re')}
              />
              <Icons
                style={styles.PasswordCheck}
                name={this.state.icon_eye2}
                color="#837F88"
                size={20}
                onPress={() => {
                  this._OnChangeShowPassword2 ();
                }}
              />
            </View>
            {this.state.check_pass ?
              <> 
                <Text style={{color: 'green'}}>
                  {this.state.textpasswordcheck}
                </Text>
                <View style={styles.bottonRegister}>
                  <LoginButton
                    title="Register"
                    onPress={() => {
                      this._OnRegister ();
                      
                    this.props.navigation.navigate ('Login');alert('Finish')
                    }}
                  />
                </View>
              </>
              : 
              <>
                <Text style={{color: 'red'}}>
                  {this.state.textpasswordcheck}
                </Text>
                <View style={styles.bottonRegister}>
                  <LoginButton
                  title="Register"
                  disabled
                  />
                </View>
              </>}

            
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create ({
  Container: {
    // justifyContent: 'center',
    flex: 1,
    // alignItems:'center'
    // marginTop: 200,
  },
  bottonRegister: {
    marginTop: 30,
    width: 300,
    justifyContent: 'center',
    marginBottom: 110,
  },
  TextInput: {
    height: 40,
    marginTop: 2,
    width: 300,
    borderRadius: 20,
    fontSize: 13,
    left: 10,
    flex: 1,
  },
  iconInput: {
    padding: 10,
    marginTop: 10,
    height: 20,
    width: 20,
    resizeMode: 'stretch',
    alignItems: 'center',
    resizeMode: 'stretch',
    left: 10,
  },
  ContainerTextInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // alignItems: 'center',
    backgroundColor: '#fff',
    height: 40,

    marginTop: 10,
    width: 300,
  },
  newHeader: {
    height: 50,
    backgroundColor: '#5D4C78',
    width: '100%',
    marginBottom: 150,
  },
  PasswordCheck: {
    marginTop: 7,
    right: 20,
  },
  ImageContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  IconCheck: {
    right: 30,
    top: 8,
  },
  TextInputCheck: {
    paddingLeft: 15,
    paddingRight: 15,
    borderColor: 'green',
    borderBottomWidth: 5,
    borderWidth: 1,
    width: 280,
    right: 5,
  },
});
export default RegisterActivity;
