import React, {Component, useState} from 'react';
import io from 'socket.io-client';
import socketIOClient from 'socket.io-client';
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Picker,
  Card,
  CardItem,
  Body,
  Button,
} from 'native-base';
import moment from 'moment';
import TimePicker from 'react-native-24h-timepicker';
import Modal from 'react-native-modal';
import Icons from 'react-native-vector-icons/FontAwesome';
import {DataTable, Switch} from 'react-native-paper';
// import { ListView } from 'react-native';
import {
  View,
  Text,
  Alert,
  Image,
  StyleSheet,
  SafeAreaView,
  Platform,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from 'react-native';
import {get, post, Delete, put} from '../Support/service';
import {event} from 'react-native-reanimated';
import Swipeout from 'react-native-swipeout-mod';
// import {ScrollView} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

var socket_client = io ('http://192.168.0.57:3434');

class GroupActivity extends Component {
  constructor (props) {
    super (props);
    this.state = {
      input: '',
      peb: '',
      message: [],
      // endpoint: 'http://192.168.0.57:3434',
      // temp: '',
      // humid: '',
      date: moment ().format ('YYYY-MM-DD'),
      timeON: moment ().format ('HH:mm'),
      timeOFF: moment ().format ('HH:mm'),
      show: false,
      isVisible: false,
      timeArray: [],
      open: false,
      switchs_ON_OFF: false,
      data_to_database: null,
      data_from_database: null,
      switchValue: false,
      action: 'off',
      selected: null,
      listViewData: null,
      client_id: [],
      group_device: false,
      group_array: [],
      group_name: null,
      group_name_to_database: null,
      edit: false,
      group_index: null,
      group_array_cancel: [],
      data_del: null,
      data_edit: null,
      
    };
  }

  componentWillMount () {
    this.get_GroupName ();
  }
  
  onValueChange = value => {
    console.log (value);
    this.setState ({
      selected: value,
    });
  };
  get_time = async () => {
    let id = this.state.selected;
    try {
      await get (`main/api/v1/time_to_app/${id}`, null).then (res => {
        if (res.success) {
          let time_array = [];
          // console.log ('res.result : ', res.result);
          this.setState (
            {
              timeArray: res.result,
            },
            () => {}
          );
        } else {
          alert (res.error_message);
        }
      });
    } catch (error) {
      alert (error);
    }
  };

  get_device = async () => {
    try {
      await get (`main/api/v1/device_info`, null).then (res => {
        if (res.success) {
          // console.log ('device_info : ', res.result);
          this.setState ({
            client_id: res.result,
          });
        } else {
          alert ('get_device', res.error_message);
        }
      });
    } catch (error) {
      alert (error);
    }
  };

  clickON () {
    let payload = {
      device_name: 'solenoid_valve_',
      device_id: '201',
      status: '{"status":"on"}',
    };
    socket_client.emit ('arduino', payload, () => {
      socket_client.close ();
    });
  }

  clickOFF () {
    let payload = {
      device_name: 'solenoid_valve_',
      device_id: '201',
      status: '{"status":"off"}',
    };

    socket_client.emit ('arduino', payload, () => {
      socket_client.close ();
    });
  }

  onTimeONCancel () {
    this.TimePicker.close ();
  }

  onTimeONConfirm (hour, minute) {
    this.setState (
      {
        timeON: `${hour}:${minute}`,
      },
      () => {}
    );
    this.TimePicker.close ();
  }

  onTimeOFFCancel () {
    this.TimePicker.close ();
  }

  onTimeOFFConfirm (hour, minute) {
    this.setState (
      {
        timeOFF: `${hour}:${minute}`,
      },
      () => {}
    );
    this.TimePicker.close ();
  }

  _onOpenModal = () => {
    this.setState ({
      isVisible: true,
    });
  };

  _onCloseModal = () => {
    this.setState ({
      isVisible: false,
    });
  };

  _SetOn = () => {
    this.TimePicker.open ();
    this.setState ({
      open: true,
    });
  };

  _SetOff = () => {
    this.TimePicker.open ();
    this.setState ({
      open: false,
    });
  };

  onSave_time = async () => {
    let data_array = [];
    let obj = {
      client_id: this.state.selected || this.state.data_device[0],
      device_critical: [
        {
          time: moment (`${this.state.date} ${this.state.timeON}`).format (
            'YYYY-MM-DD HH:mm:ss'
          ),
          action: 'on',
        },
        {
          time: moment (`${this.state.date} ${this.state.timeOFF}`).format (
            'YYYY-MM-DD HH:mm:ss'
          ),
          action: 'off',
        },
      ],
    };
    console.log ('obj : ', obj);
    data_array.splice (0, 0, obj);

    this.state.timeArray.splice (0, 0, obj.device_critical[0]);
    this.state.timeArray.splice (0, 0, obj.device_critical[1]);
    // console.log ('timeArray : ', this.state.timeArray);
    this.setState (
      {
        data_to_database: data_array[0],
        isVisible: false,
        timeArray: this.state.timeArray,
      },
      () => {
        // console.log ('timeArray : ', this.state.timeArray);
        this.onSave_to_database ();
      }
    );
  };

  onSave_to_database = async () => {
    let object = this.state.data_to_database;

    try {
      await post (object, 'main/devices', null).then (res => {
        if (res.success) {
        } else {
          alert (res.error_message);
        }
      });
    } catch (error) {
      alert (error);
    }
    // console.log ('devices' + this.state);
  };
  _onOpenModalGroup = () => {
    this.setState ({
      group_device: true,
    });
  };

  _onCloseModalGroup = () => {
    this.setState ({
      group_device: false,
    });
  };

  set_group_name = event => {
    this.setState ({
      group_name: event,
    });
  };

  onSave_group = async () => {
    let data = this.state.group_array;

    data.push ({
      group_name: this.state.group_name,
    });

    this.setState (
      {
        group_array: data,
        group_name_to_database: this.state.group_name,
      },
      () => {
        console.log ('group', this.state.group_array);
        this.SaveGroup_to_database ();
      }
    );
    this._onCloseModalGroup ();
  };

  SaveGroup_to_database = async () => {
    let obj = {group_name: this.state.group_name_to_database};
    console.log (obj);
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        post (obj, 'api/v1/group/group_data', user_token).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  get_GroupName = async () => {
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        get ('api/v1/group/group_data', user_token).then (res => {
          if (res.success) {
            // console.log('res',res.result)
            this.setState ({
              group_array: res.result,
            });
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  onRemove = index_group => {
    let data = this.state.group_array;
    let data_del = data[index_group];
    data.splice (index_group, 1);
    // console.log(data_del)
    this.setState (
      {
        group_array: data,
        data_del: data_del,
      },
      () => {
        this.onRemoveDatabase ();
      }
    );
  };

  onRemoveDatabase = async () => {
    let obj = this.state.data_del;
    console.log (obj);
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        Delete (obj, 'api/v1/group/group_data', user_token).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  onRemoveAlert = index => {
    let index_group = index;
    Alert.alert (
      'คุณต้องการจะลบหรือไม่ ?',
      'กด ตกลง เพื่อลบ',
      [
        {
          text: 'ยกเลิก',
          onPress: () => console.log ('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'ตกลง', onPress: () => this.onRemove (index_group)},
      ],
      {cancelable: false}
    );
  };

  onEditGroupname = (element, index) => {
    let data = element;
    let data_cancel = JSON.stringify (this.state.group_array);
    this.setState ({
      edit: !this.state.edit,
      group_index: index,
      group_array_cancel: data_cancel,
    });
  };

  onEditChange = (event, index) => {
    let data = this.state.group_array;
    data[index].group_name = event;
    // console.log(data[index])
    this.setState ({
      group_array: data,
      data_edit: data[index],
    });
  };

  onEditSave = () => {
    this.setState (
      {
        edit: !this.state.edit,
      },
      () => {
        this.onEditSaveDatabase ();
      }
    );
  };

  onEditSaveDatabase = async () => {
    let obj = this.state.data_edit;
    console.log (obj);
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        put (obj, 'api/v1/group/group_data', user_token).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  onEditCancel = () => {
    let data_cancel = JSON.parse (this.state.group_array_cancel);

    this.setState ({
      edit: !this.state.edit,
      group_array: data_cancel,
    });
  };
  render () {
    const {input, message} = this.state;
    // {this.state.timeArray ((element))}
    return (   
      <SafeAreaView style={{backgroundColor: 'white', height: 700}}>      
        <Header style={{backgroundColor: '#f4fcf2'}} />            
        <Text style={{fontSize: 18, left: 170, bottom:40, color: '#45A46B'}}>
          Group
        </Text>
        <Icons
          style={{fontSize: 25, left: 350, bottom: 65, color: '#45A46B'}}
          name="plus-square"
          onPress={this._onOpenModalGroup} />                
       <ScrollView style={{marginBottom:100}}>       
        {this.state.group_array
          ? this.state.group_array.map ((element, index) => {
              {                
              }
              return (                
                <View>
                  {!this.state.edit
                    ? <Swipeout
                        style={{backgroundColor: 'white'}}
                        autoClose={true}
                        right={[
                          {
                            component: (
                              <View
                                style={{backgroundColor: 'white', flex: 1}}
                              >
                                <Icons
                                  style={{
                                    fontSize: 40,
                                    color: '#4A4A4A',
                                    left: 20,
                                    marginTop: 40,
                                  }}
                                  name="cog"
                                  onPress={() => {
                                    this.onEditGroupname (element, index);
                                  }}
                                />
                              </View>
                            ),
                          },
                          {
                            component: (
                              <View
                                style={{backgroundColor: 'white', flex: 1}}
                              >
                                <Icons
                                  style={{
                                    fontSize: 40,
                                    color: '#cf3e3e',
                                    left: 20,
                                    marginTop: 40,
                                  }}
                                  name="trash"
                                  onPress={() => {
                                    this.onRemoveAlert (index);
                                  }}
                                />
                              </View>
                            ),
                          },
                        ]}>              
                        <View style={{marginBottom: 5}}>
                          <Card style={styles.cardStyle}>

                            <CardItem style={{backgroundColor: '#45A46B'}}>
                              <Icons
                                style={{
                                  color: 'white',
                                  fontSize: 50,
                                  left: 300,
                                }}
                                name="angle-left"
                              />
                              <Body>
                                <Text style={{margin: 10, color: 'white',fontSize:18,width:'80%',height:30}} onPress={() => this.props.navigation.navigate("Group",{screen: 'Group', params: {data: element}})}>
                                  Group name : {element.group_name}
                                </Text>
                              </Body>
                            </CardItem>
                          </Card>
                        </View>                      
                      </Swipeout>
                    : <View>
                        {this.state.group_index === index
                          ? <View>
                              <View style={{marginBottom: 5}}>
                                <Card style={styles.cardStyle2}>
                                  <CardItem
                                    style={{backgroundColor: '#45A46B7'}}
                                  >
                                    <Body>
                                      <Text style={{color: 'white', left: 130}}>
                                        {' '}Group Name :
                                      </Text>
                                      <TextInput
                                        style={{width: 150, left: 110}}
                                        placeholderTextColor="white"
                                        maxLength={30}
                                        underlineColorAndroid="#C5C5C5"
                                        type="text"
                                        color="white"
                                        onChangeText={event =>
                                          this.onEditChange (event, index)}
                                      />
                                      <View style={{bottom: 20}}>
                                        <Icons
                                          style={{
                                            fontSize: 20,
                                            color: 'white',
                                            left: 230,
                                          }}
                                          name="check"
                                          onPress={() => this.onEditSave ()}
                                        />
                                        <Icons
                                          style={{
                                            fontSize: 20,
                                            color: 'red',
                                            left: 230,
                                          }}
                                          name="times"
                                          onPress={() => this.onEditCancel ()}
                                        />
                                      </View>

                                    </Body>
                                  </CardItem>

                                </Card>

                              </View>
                            </View>
                          : <View>
                              <View style={{marginBottom: 5}}>
                                <Card style={styles.cardStyle}>
                                  <CardItem
                                    style={{backgroundColor: '#45A46B'}}
                                  >
                                    <Body>
                                      <Text
                                        style={{margin: 10, color: 'white'}}
                                      >
                                        Group name : {element.group_name}
                                      </Text>
                                    </Body>
                                  </CardItem>
                                </Card>
                              </View>
                            </View>}

                      </View>}
                </View>
              );
            })
          : null}
          <Modal
            isVisible={this.state.group_device}
            style={{
              backgroundColor: '#f4fcf2',
              width: 350,
              borderRadius: 20,
              left: 10,
            }}
          >
            <Icons
              style={{
                fontSize: 25,
                left: 300,
                marginTop: 10,
                color: '#2C8D5D',
              }}
              onPress={this._onCloseModalGroup}
              name="times-circle"
            />
            <Content style={{marginTop: 20}}>
              <Card
                style={{
                  width: 250,
                  left: 50,
                  left: 50,
                  borderTopLeftRadius: 8,
                  borderTopRightRadius: 8,
                  padding: 10,
                  borderBottomLeftRadius: 8,
                  borderBottomRightRadius: 8,
                  marginBottom: 5,
                }}
              >
                <CardItem>
                  <Body>
                    <Text style={{left: 50}}>
                      Group name{' '}
                    </Text>
                    <View style={{width: 100}}>

                      <TextInput
                        style={{width: 200}}
                        placeholderTextColor="#A6A6A6"
                        maxLength={30}
                        underlineColorAndroid="#C5C5C5"
                        type="text"
                        selectionColor="white"
                        onChangeText={event => this.set_group_name (event)}
                      />
                    </View>

                  </Body>
                </CardItem>
              </Card>
            </Content>
            <View>
              <TouchableOpacity
                style={styles.buttonSave}
                onPress={() => {
                  this.onSave_group ();
                }}
              >
                <Text style={styles.buttonTextSave}>save</Text>
              </TouchableOpacity>
            </View>

          </Modal>

        <Modal
          isVisible={this.state.isVisible}
          style={{backgroundColor: 'white'}}
        >
          <Icons
            style={{
              fontSize: 25,
              left: 330,
              bottom: 30,
              color: 'black',
            }}
            onPress={this._onCloseModal}
            name="times-circle"
          />
          <View>
            <View>
              <TouchableOpacity onPress={this._SetOn} style={styles.buttonOpen}>
                <Text style={styles.buttonText}>ตั้งเวลาเปิด</Text>

              </TouchableOpacity>

            </View>

            <View>
              <TouchableOpacity onPress={this._SetOff} style={styles.buttonOff}>
                <Text style={styles.buttonText}>ตั้งเวลาปิด</Text>

              </TouchableOpacity>

              <Text style={styles.textOpen}>{this.state.timeON}</Text>

              <Text style={styles.textOff}>{this.state.timeOFF}</Text>

              <TouchableOpacity
                onPress={() => this.onSave_time ()}
                style={styles.buttonSave}
              >
                <Text style={styles.buttonTextSave}>save</Text>
              </TouchableOpacity>

            </View>
          </View>
          {this.state.open
            ? <View>
                <TimePicker
                  ref={ref => {
                    this.TimePicker = ref;
                  }}
                  onCancel={() => this.onTimeONCancel ()}
                  onConfirm={(hour, minute) =>
                    this.onTimeONConfirm (hour, minute)}
                />
              </View>
            : <View>
                <TimePicker
                  ref={ref => {
                    this.TimePicker = ref;
                  }}
                  onCancel={() => this.onTimeOFFCancel ()}
                  onConfirm={(hour, minute) =>
                    this.onTimeOFFConfirm (hour, minute)}
                />
              </View>}
        </Modal>  
         </ScrollView>
      </SafeAreaView>
   
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    marginTop: 100,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    bottom: 50,
  },

  textOpen: {
    fontSize: 20,
    marginTop: 10,
    left: 70,
    bottom: 180,
  },
  textOff: {
    fontSize: 20,
    marginTop: 10,
    left: 240,
    bottom: 217,
  },
  buttonOpen: {
    backgroundColor: '#4EB151',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 150,
    left: 30,
  },
  buttonOff: {
    backgroundColor: 'red',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 150,
    left: 200,
    bottom: 145,
  },
  buttonSave: {
    backgroundColor: '#2C8D5D',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 100,
    justifyContent: 'center',
    left: 140,
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 20,
  },
  buttonTextSave: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 18,
  },
  DataTableRowStyle: {
    backgroundColor: '#DAF7A6',
    paddingLeft: 15,
    paddingRight: 15,
    borderColor: 'green',
    borderWidth: 1,
  },
  TextInput: {
    height: 40,
    marginBottom: 20,
    width: 300,
    borderRadius: 20,
    fontSize: 14,
    left: 15,
    flex: 1,
  },
  ContainerTextInput: {
    flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'center',
    height: 20,
    borderRadius: 10,
    marginBottom: 10,
    width: 120,
    right: 50,
  },
  cardStyle: {
    padding: 10,
    marginBottom: 5,
    backgroundColor: '#45A46B',
    borderRadius: 10,
  },
  cardStyle2: {
    height: 100,
    marginBottom: 5,
    backgroundColor: '#45A46B',
    borderRadius: 10,
  },
});
export default GroupActivity;
