import React, {Component} from 'react';
import {
  View,
  Text,
  Alert,
  Image,
  StyleSheet,
  SafeAreaView,
  TextInput,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Content, Header} from 'native-base';
import {ip} from '../Support/contance';
import Icons from 'react-native-vector-icons/FontAwesome';
import {get, post, Delete, put} from '../Support/service';
import AsyncStorage from '@react-native-community/async-storage';
import Swipeout from 'react-native-swipeout-mod';
import {Card, CardItem, Body, Picker, Form} from 'native-base';
import Modal from 'react-native-modal';
import {DataTable, Switch} from 'react-native-paper';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import io from 'socket.io-client';
import TimePicker from 'react-native-simple-time-picker';

var socket_client = io ('http://192.168.0.57:3434');

class DeviceNameActivity extends Component {
  constructor () {
    super ();
    this.state = {
      data_group: null,
      device_array: [],
      edit: false,
      device_index: null,
      device_array_cancel: null,
      data_del: null,
      data_edit: null,
      isVisible_solenoid: false,
      isVisible_sht20: false,
      switch_ON_OFF: false,
      switchValue: false,
      selected: null,
      data_modal: null,
      data_modal_solenoid: null,
      action: 'off',
      device_type_array: [],
      device_type_want: null,
      values: [0, 50],
      action1: 'off',
    };
  }

  componentDidMount () {
    // console.log("data : ",this.props.route.params.data)
    this.setState (
      {
        data_group: this.props.route.params.data,
      },
      () => {
        this.get_device ();
      }
    );
  }

  get_device = async () => {
    // console.log("data_group : ",this.state.data_group.group_id)
    let group_id = this.state.data_group.group_id;
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        get (`api/v1/device/device_data/${group_id}`, user_token).then (res => {
          if (res.success) {
            let device_array = [];
            res.result.map (element => {
              if (element.device_type == 'solenoid_valve') {
                device_array.push ({
                  ...element,
                  device_critical: JSON.parse (element.device_critical),
                });
              } else {
                device_array.push ({
                  ...element,
                });
              }
            });

            // console.log ('res', device_array);
            this.setState (
              {
                device_array: device_array,
              },
              () => {
                console.log ('device_array : ', this.state.device_array);
              }
            );
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  get_device_type = async () => {
    let type = JSON.stringify (this.state.device_type_want);
    // console.log ('type : ', type);
    try {
      await get (`api/v1/device/device_type/${type}`, null).then (res => {
        if (res.success) {
          // console.log ('res', res.result);
          this.setState (
            {
              device_type_array: res.result,
            },
            () => {
              // console.log ('device_array : ', this.state.device_array);
            }
          );
        } else {
          alert (res.error_message);
        }
      });
    } catch (error) {
      alert (error);
    }
  };

  onEditDevice = index => {
    let data_cancel = JSON.stringify (this.state.device_array);
    this.setState ({
      edit: !this.state.edit,
      device_index: index,
      device_array_cancel: data_cancel,
    });
  };

  onEditChange = (event, index) => {
    let data = this.state.device_array;
    data[index].device_name = event;
    // console.log(data[index])
    this.setState ({
      device_array: data,
      data_edit: data[index],
    });
  };

  onEditSave = () => {
    this.setState (
      {
        edit: !this.state.edit,
      },
      () => {
        this.onEditSaveDatabase ();
      }
    );
  };

  onEditSaveDatabase = async () => {
    let obj = this.state.data_edit;
    // console.log (obj);
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        put (obj, 'api/v1/device/device_data', user_token).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  onEditCancel = () => {
    let data_cancel = JSON.parse (this.state.device_array_cancel);

    this.setState ({
      edit: !this.state.edit,
      device_array: data_cancel,
    });
  };

  onRemoveAlert = index => {
    let device_index = index;
    Alert.alert (
      'คุณต้องการจะลบหรือไม่ ?',
      'กด ตกลง เพื่อลบ',
      [
        {
          text: 'ยกเลิก',
          onPress: () => console.log ('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'ตกลง', onPress: () => this.onRemove (device_index)},
      ],
      {cancelable: false}
    );
  };

  onRemove = device_index => {
    let data = this.state.device_array;
    let data_del = data[device_index];
    data.splice (device_index, 1);
    // console.log(data_del)
    this.setState (
      {
        device_array: data,
        data_del: data_del,
      },
      () => {
        this.onRemoveDatabase ();
      }
    );
  };

  onRemoveDatabase = async () => {
    let obj = this.state.data_del;
    // console.log (obj);
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        put (obj, 'api/v1/device/device_data_clear', user_token).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  // _onOpenModal = element => {
  //   this.setState (
  //     {
  //       isVisible: true,
  //     },
  //     () => {
  //       this.render_switch (element);
  //     }
  //   );
  // };

  _onCloseModal = () => {
    // console.log ('1');
    this.setState ({
      isVisible_solenoid: false,
      isVisible_sht20: false,
    });
  };
  set_Valve_to_DBMS = async () => {
    let device_critical = {
      option: this.state.selected,
      max_value: this.state.values[1],
      min_value: this.state.values[0],
    };

    let obj = {
      action: this.state.action,
      device_critical: device_critical,
      client_id: this.state.data_modal_solenoid.client_id,
    };
    console.log ('obj : ', obj);
    try {
      AsyncStorage.getItem ('user_token').then (user_token => {
        put (
          obj,
          'api/v1/device/device_activity_data',
          user_token
        ).then (res => {
          if (res.success) {
          } else {
            alert (res.error_message);
          }
        });
      });
    } catch (error) {
      alert (error);
    }
  };

  render_switch = element => {
    switch (element.device_type) {
      case 'solenoid_valve':
        console.log ('Solenoid');
        let data = ['temp', 'humid'];
        this.setState (
          {
            isVisible_solenoid: true,
            data_modal_solenoid: element,
            device_type_want: data,
          },
          () => {
            this.get_device_type ();
          }
        );
        break;
      case 'sht20':
        console.log ('Sht20');
        this.setState ({
          isVisible_sht20: true,
          isVisible_solenoid: false,
          data_modal: element,
        });
        break;
    }
  };

  enableScroll = () => this.setState ({scrollEnabled: true});
  disableScroll = () => this.setState ({scrollEnabled: false});

  multiSliderValuesChange = values => {
    this.setState ({
      values,
    });
  };

  clickOFF () {
    let payload = {
      device_name: 'solenoid_valve_',
      device_id: '201',
      status: '{"status":"off"}',
    };

    socket_client.emit ('arduino', payload, () => {
      socket_client.close ();
    });
  }

  clickON () {
    let payload = {
      device_name: 'solenoid_valve_',
      device_id: '201',
      status: '{"status":"on"}',
    };
    // console.log ('payload', payload);
    socket_client.emit ('arduino', payload, () => {
      socket_client.close ();
    });
  }

  clickOFF2 = async () => {
    let payload = {
      client_id: 'solenoid_valve_201',
      action: 'off',
    };
    try {
      await post (payload, 'api/v1/water/solenoid_on_off').then (res => {
        console.log ('res.result', res.result);
        if (res.success) {
        } else {
          alert (res.error_message);
        }
      });
    } catch (error) {
      alert (error);
    }
  };

  clickON2 = async () => {
    let payload = {
      client_id: 'solenoid_valve_201',
      action: 'on',
    };
    try {
      await post (payload, 'api/v1/water/solenoid_on_off').then (res => {
        // console.log ('res.result', res.result);
        if (res.success) {
        } else {
          alert (res.error_message);
        }
      });
    } catch (error) {
      alert (error);
    }
  };

  render () {
    return (
      <View style={{backgroundColor: 'white', height: '100%'}}>
        <Header style={{backgroundColor: '#f4fcf2'}} />
        <Text style={{fontSize: 18,bottom: 40, color: '#45A46B',textAlign:'center'}}>
          {this.state.data_group ? this.state.data_group.group_name : null}
        </Text>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            bottom: 65,
          }}
        >
          <Icons
            style={{fontSize: 25, color: '#2C8D5D'}}
            name="chevron-left"
            onPress={() => this.props.navigation.navigate ('Main')}
          />
          <Icons
            style={{fontSize: 25, color: '#2C8D5D'}}
            name="plus"
            onPress={() =>
              this.props.navigation.navigate ('Device', {
                screen: 'Device',
                params: {data: this.state.data_group},
              })}
          />
        </View>
        <ScrollView>

          {this.state.device_array.map ((element, index) => {
            return (
              <View>
                {!this.state.edit
                  ? <Swipeout
                      style={{backgroundColor: 'white'}}
                      autoClose={true}
                      right={[
                        {
                          component: (
                            <View style={{backgroundColor: 'white', flex: 1}}>
                              <Icons
                                style={{
                                  fontSize: 40,
                                  color: '#556b4f',
                                  left: 20,
                                  marginTop: 40,
                                }}
                                name="cog"
                                onPress={() => {
                                  this.onEditDevice (index);
                                }}
                              />
                            </View>
                          ),
                        },
                        {
                          component: (
                            <View style={{backgroundColor: 'white', flex: 1}}>
                              <Icons
                                style={{
                                  fontSize: 40,
                                  color: '#cf3e3e',
                                  left: 20,
                                  marginTop: 40,
                                }}
                                name="trash"
                                onPress={() => {
                                  this.onRemoveAlert (index);
                                }}
                              />
                            </View>
                          ),
                        },
                      ]}
                    >
                      <View style={{marginBottom: 5}}>
                        <Card style={styles.cardStyle}>

                          <CardItem style={{backgroundColor: '#45A46B'}}>
                            <Icons
                              style={{
                                color: 'white',
                                fontSize: 50,
                                left: 300,
                              }}
                              name="angle-left"
                            />
                            <Body>
                              <Text
                                style={{
                                  margin: 10,
                                  color: 'white',
                                  fontSize: 18,
                                  width: '80%',
                                  height: 40,
                                  justifyContent: 'center',
                                }}
                                onPress={() => this.render_switch (element)}
                              >
                                Device name : {element.device_name}
                              </Text>
                            </Body>
                          </CardItem>
                        </Card>
                      </View>
                    </Swipeout>
                  : <View>
                      {this.state.device_index === index
                        ? <View>
                            <View style={{marginBottom: 5}}>
                              <Card style={styles.cardStyle2}>
                                <CardItem style={{backgroundColor: '#45A46B7'}}>
                                  <Body>
                                    <Text style={{color: 'white', left: 130}}>
                                      {' '} Group Name :
                                    </Text>
                                    <TextInput
                                      style={{width: 150, left: 110}}
                                      placeholderTextColor="white"
                                      maxLength={30}
                                      underlineColorAndroid="#C5C5C5"
                                      type="text"
                                      color="white"
                                      onChangeText={event =>
                                        this.onEditChange (event, index)}
                                    />
                                    <View style={{bottom: 20}}>
                                      <Icons
                                        style={{
                                          fontSize: 20,
                                          color: 'white',
                                          left: 230,
                                        }}
                                        name="check"
                                        onPress={() => this.onEditSave ()}
                                      />
                                      <Icons
                                        style={{
                                          fontSize: 20,
                                          color: 'red',
                                          left: 230,
                                        }}
                                        name="times"
                                        onPress={() => this.onEditCancel ()}
                                      />
                                    </View>

                                  </Body>
                                </CardItem>

                              </Card>

                            </View>
                          </View>
                        : <View>
                            <View style={{marginBottom: 5}}>
                              <Card style={styles.cardStyle}>
                                <CardItem style={{backgroundColor: '#45A46B'}}>
                                  <Body>
                                    <Text style={{margin: 10, color: 'white'}}>
                                      Group name : {element.device_name}
                                    </Text>
                                  </Body>
                                </CardItem>
                              </Card>
                            </View>
                          </View>}
                    </View>}

                {/* Solenoid */}
                <Modal
                  isVisible={this.state.isVisible_solenoid}
                  style={{
                    backgroundColor: 'white',
                    borderRadius: 20,
                  }}
                >
                  <Icons
                    style={{
                      fontSize: 25,
                      left: 330,
                      bottom: 70,
                      color: '#2C8D5D',
                    }}
                    onPress={() => {
                      this._onCloseModal ();
                    }}
                    name="times-circle"
                  />
                  <View>

                    <Text style={{bottom: 80, left: 150}}>
                      {this.state.data_modal_solenoid
                        ? this.state.data_modal_solenoid.device_name
                        : null}
                    </Text>

                    <Text style={{bottom: 50, left: 160}}>
                      AUTO
                    </Text>
                    <Text style={{bottom: 30, left: 220}}>
                      {this.state.action}
                    </Text>
                    <Switch
                      style={{bottom: 50, right: 250, scaleY: 1.5, scaleX: 1.5}}
                      trackColor={{true: '#45A46B', false: '#F2F7F4'}}
                      onValueChange={switchs_ON_OFF => {
                        this.state.action = switchs_ON_OFF ? 'auto' : 'off';
                        this.setState ({action: this.state.action}, () => {
                          // this.state.action == 'on'
                          //   ? this.clickON ()
                          //   : this.clickOFF ();
                        });
                      }}
                      value={this.state.action == 'auto' ? true : false}
                    />

                    <View style={{bottom: 30, width: 250, left: 70}}>
                      <Picker
                        mode="dropdown"
                        iosIcon={<Icons name="arrow-down" />}
                        placeholder="Select your Device"
                        placeholderStyle={{color: '#bfc6ea'}}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.selected}
                        onValueChange={value => {
                          this.setState (
                            {
                              selected: value,
                            },
                            () => {
                              // console.log (this.state.selected);
                            }
                          );
                        }}
                      >
                        {this.state.device_type_array.map (element => {
                          return (
                            <Picker.Item
                              label={element.device_type_name}
                              value={element.device_type}
                            />
                          );
                        })}
                      </Picker>
                    </View>
                    <ScrollView
                      scrollEnabled={this.state.scrollEnabled}
                      style={{left: 50, bottom: 30}}
                    >
                      {console.log (
                        this.state.data_modal_solenoid
                          ? this.state.data_modal_solenoid.device_critical
                          : null
                      )}
                      <MultiSlider
                        values={[
                          this.state.data_modal_solenoid
                            ? this.state.data_modal_solenoid.device_critical
                                .min_value
                            : null,
                          this.state.data_modal_solenoid
                            ? this.state.data_modal_solenoid.device_critical
                                .max_value
                            : null,
                        ]}
                        sliderLength={280}
                        onValuesChange={this.multiSliderValuesChange}
                        min={0}
                        max={50}
                        step={1}
                      />

                      <Text>{this.state.values[0]}</Text>
                      <Text style={{left: 265, bottom: 20}}>
                        {this.state.values[1]}
                      </Text>
                    </ScrollView>
                  </View>
                  <TouchableOpacity
                    onPress={() => {
                      this.set_Valve_to_DBMS ();
                    }}
                    style={styles.buttonSave}
                  >
                    <Text style={styles.buttonTextSave}>save</Text>
                  </TouchableOpacity>

                  {/* `เปิด/ปิดแบบกำนหดเอง */}
                  <Text style={{left: 250, bottom: 25}}>
                    {this.state.action1}
                  </Text>

                  <Switch
                    style={{right: 230, scaleY: 1.5, scaleX: 1.5, bottom: 50}}
                    trackColor={{false: '#CCCFDA', true: 'green'}}
                    onValueChange={switchs_ON_OFF2 => {
                      this.state.action1 = switchs_ON_OFF2 ? 'on' : 'off';
                      this.setState ({action1: this.state.action1}, () => {
                        this.state.action1 == 'on'
                          ? this.clickON2 ()
                          : this.clickOFF2 ();
                      });
                    }}
                    value={this.state.action1 == 'on' ? true : false}
                  />
                  <View>
                    <TouchableOpacity
                      style={{
                        backgroundColor: 'black',
                        marginTop: 5,
                        width: 150,
                        height: 30,
                        left: 110,
                        bottom: 10,
                        borderRadius: 10,
                      }}
                      onPress={() =>
                        this.props.navigation.navigate (
                          'Settime',
                          {
                            screen: 'Settime',
                            params: {
                              data_device: this.state.data_modal_solenoid,
                              data_group: this.state.data_group,
                            },
                          },
                          this._onCloseModal ()
                        )}
                    >
                      <Text style={{left: 55, marginTop: 2, color: 'white'}}>
                        ตั้งเวลา
                      </Text>
                    </TouchableOpacity>
                  </View>
                </Modal>

                {/* Sht20 */}
                <Modal
                  isVisible={this.state.isVisible_sht20}
                  style={{
                    backgroundColor: 'white',
                    borderRadius: 20,
                  }}
                >
                  <Icons
                    style={{
                      fontSize: 25,
                      left: 335,
                      bottom: 190,
                      color: '#2C8D5D',
                    }}
                    onPress={() => {
                      this._onCloseModal ();
                    }}
                    name="times-circle"
                  />
                  <View>
                    <Text style={{bottom: 200, left: 160}} />
                  </View>
                </Modal>

              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create ({
  cardStyle: {
    padding: 10,
    marginBottom: 5,
    backgroundColor: '#45A46B',
    borderRadius: 10,
  },
  cardStyle2: {
    height: 100,
    marginBottom: 5,
    backgroundColor: '#45A46B',
    borderRadius: 10,
  },
  buttonSave: {
    backgroundColor: '#2C8D5D',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 100,
    justifyContent: 'center',
    left: 140,
    bottom: 60,
  },
  buttonTextSave: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 18,
  },
  buttonTextON: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '600',
    left: 18,
  },
  buttonON: {
    backgroundColor: '#2C8D5D',
    paddingVertical: 11,
    paddingHorizontal: 17,
    borderRadius: 50,
    marginVertical: 50,
    width: 100,
    justifyContent: 'center',
    left: 140,
  },
});
export default DeviceNameActivity;
