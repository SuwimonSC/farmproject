import React, {Component} from 'react';
import io from 'socket.io-client';
import {
  View,
  Text,
  Alert,
  Image,
  StyleSheet,
  SafeAreaView,
  ImageBackground,
} from 'react-native';
import {Header} from 'native-base';
import {CameraBotton} from '../Components/CameraButton';
import {ip} from '../Support/contance';
// import Video from 'react-native-video';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {DataTable, Switch} from 'react-native-paper';
// import VideoPlayer from 'react-native-video-player';
import Video from 'react-native-video';
// import RtspPlayer from 'react-native-pxplayer'
// import { VLCPlayer, VlCPlayerView } from 'react-native-vlc-player';
import VlcPlayer from 'react-native-vlc-player';
import {LivePlayer} from 'react-native-live-stream';

var socket_client = io ('http://192.168.0.57:3434');

class CameraActivity extends Component {
  vlcplayer = React.createRef ();
  constructor () {
    super ();
    this.state = {
      imgUrl: '',
      action: 'off',
      stream: '',
      duration: '',
    };
  }

  componentDidMount () {
    this.componentPath ();
    // console.log (this.vlcplayer);
    // this.componentPathStream ();
  }

  CameraSnap () {
    // console.log('aom')
    let payload = {
      devices: 'farm/came_1',
      action: 'snapshot',
    };
    socket_client.emit ('camera_control', payload, () => {
      socket_client.close ();
      console.log (payload);
    });
  }

  componentPath = () => {
    socket_client.on ('camera_snapshot', res => {
      // console.log ('data : ', res.path_image);
      console.log ('data : ', res);
      if (res.image_path) {
        this.setState ({
          imgUrl: res.image_path,
        });
      }
    });
  };

  // componentPathStream = () => {
  //   socket_client.on ('camera_stream', res => {
  //     // console.log ('data : ', res.path_image);
  //     console.log ('data : ', res);
  //     if (res.info) {
  //       this.setState ({
  //         stream: res.info,
  //       });
  //     }
  //   });
  // };

  clickON = async () => {
    let payload = {
      topic: 'farm/came_1/image',
      action: 'on',
    };

    console.log ('payload', payload);

    socket_client.emit ('camera_stream', payload, () => {
      socket_client.close ();
    });
  };

  clickOFF = async () => {
    let payload = {
      topic: 'farm/came_1/image',
      action: 'off',
    };
    console.log ('payload', payload);
    socket_client.emit ('camera_stream', payload, () => {
      socket_client.close ();
    });
  };
  onLoad (data) {
    this.setState ({duration: data.duration});
  }
  render () {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground
          source={require ('../images/bgCamera.png')}
          style={styles.background}
        >
          <View
            style={{
              alignItems: 'center',
              marginTop: 150,
            }}
          >

            {/* <Text>{this.state.imgUrl}</Text> */}
            {/* {this.renderVideo} */}
            {/* <Text style={{left: 150, margin: 50}}>
              {this.state.action}
            </Text> */}
            {/* <Switch
            style={{scaleY: 1.5, scaleX: 1.5, left: 110,bottom:65}}
            trackColor={{true: '#45A46B', false: 'gray'}}
            onValueChange={switchs_ON_OFF => {
              this.state.action = switchs_ON_OFF ? 'on' : 'off';
              this.setState ({action: this.state.action}, () => {
                this.state.action == 'on' ? this.clickON () : this.clickOFF ();
              });
            }}
            value={this.state.action == 'on' ? true : false}
          /> */}
            {/* <LivePlayer
            style={styles.imagestyle}
              source={{uri: 'http://192.168.0.53:8554'}}
              ref={ref => {
                this.player = ref;
              }}
              // style={styles.video}
              paused={false}
              muted={false}
              bufferTime={300}
              maxBufferTime={1000}
              resizeMode={'contain'}
              // onLoading={() => {}}
              // onLoad={() => {}}
              // onEnd={() => {}}
            /> */}

            {/* <Video
              style={styles.imagestyle}
              fullscreenOrientation="all"
              onBuffer={this.onBuffer} // Callback function
              onError={this.videoError}
              source={require ('../video/video.mp4')}
              // source={{uri :"http://192.168.0.53:8554/unicast.m3u8"}}
              resizeMode="contain"
              rate={1}
              volume={1}
              muted={false}
              ignoreSilentSwitch={null}
              repeat={false}
              controls={true}
            /> */}
            {/* <Image 
            style={styles.imagestyle}
              source={{uri:"https://192.168.0.53/cgi-bin/currentpic.cgi?"}}
            /> */}
            <VlcPlayer
              width={640}
              height={320}
              
              ref={this.vlcplayer}
              style={styles.imagestyle}
              paused={false}
              autoplay={true}
              source={{
                uri: 'rtsp://192.168.1.140:8554/unicast',
                autoplay: true,
                initOptions: ['--rtsp-tcp'],
              }}
              hwDecoderEnabled={1}
              hwDecoderForced={1}
              mediaOptions={{
                ':network-caching': 300,
                ':live-caching': 150,
              }}
              
            />

            <Image
              style={styles.imagestyle}
              source={{uri: ip + this.state.imgUrl}}
            />
            <View
              style={{
                marginTop: 10,
                alignItems: 'center',
              }}
            >
              <TouchableOpacity
                style={styles.bottonStyle}
                title="Snap shot"
                color="purple"
                onPress={() => {
                  this.CameraSnap ();
                }}
              >
                <Text style={{color: 'white'}}>SnapShot</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}
export default CameraActivity;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#D0D0D0',
  },
  Viewstyle: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  bottonStyle: {
    // width: 50,
    width: 320,
    height: 40,
    backgroundColor: '#2C8D5D',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 200,
    borderRadius: 150,
    bottom: 20,
  },
  imagestyle: {
    width: 340,
    marginTop: 15,
    marginLeft: 30,
    paddingVertical: 50,
    borderWidth: 1,
    borderColor: '#20232a',
    borderRadius: 30,
    backgroundColor: '#f0f8ff',
    height: 210,
    right: 15,
    bottom: 50,
  },
  background: {
    flex: 1,
    width: 420,
    height: 850,
    bottom: 60,
  },
  Video: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'grey',
  },
  Videostyle: {
    width: 340,
    height: 210,
    // right: 15,
    // bottom: 50,
  },
});
