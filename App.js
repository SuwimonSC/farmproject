/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icons from 'react-native-vector-icons/FontAwesome';
import RegisterActivity from './src/Activity/RegisterActivity';
import LoginActivity from './src/Activity/LoginActivity';
import MainActivity from './src/Activity/MainActivity';
import CameraActivity from './src/Activity/CameraActivity';
import ProfileActivity from './src/Activity/ProfileActivity';
import GroupActivity from './src/Activity/GroupActivity';
import io from 'socket.io-client';
import DeviceNameActivity from './src/Activity/DeviceNameActivity';
import DeviceActivity from './src/Activity/DeviceActivity';
import SettimeActivity from './src/Activity/SettimeActivity';
import AdimActivity from './src/AdminActivity/AdminActivity';
import AdminProfileActivity from './src/AdminActivity/AdminProfileActivity';
import UserInfomation from './src/AdminActivity/UserInfomation';
import DeviceType from './src/AdminActivity/DeviceType';
import MoreDeviceActivity from './src/Activity/MoreDeviceActivity';
import DeviceDataActivity from './src/AdminActivity/DeviceDataActivity';
import EditDeviceActivity from './src/AdminActivity/EditDeviceActivity';
import AddDevice from './src/AdminActivity/AdminActivity';
import {View} from 'native-base';

import SplashActivity from './src/Activity/SplashActivity';

var socket_client = io ('http://172.20.10.3:3434');
var user_token;

const Stack = createStackNavigator ();
const Tab = createBottomTabNavigator ();
// AsyncStorage.removeItem('user_token');

var jwt_decode = require ('jwt-decode');

class MoreDevice extends Component {
  render () {
    return (
      <Stack.Navigator>
        <Stack.Screen  name="MoreDevice" component={MoreDeviceActivity} />
        
      </Stack.Navigator>
    );
  }
}
class SeeMore extends Component {
  render () {
    return (
      <Stack.Navigator>
        <Stack.Screen  name="SeeMore" component={UserInfomation} />
      </Stack.Navigator>
    );
  }
}
class Splash extends Component {
  render () {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Splash" component={SplashActivity} />
      </Stack.Navigator>
    );
  }
}

class Admin extends Component {
  render () {
    return (
      <Tab.Navigator 
       screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            let iconStyle;

            if (route.name === 'User') {
              iconName = focused ? 'cog' : 'cog';
              size = 25;
              }else if (route.name === 'Profile') {
              iconName = focused ? 'user' : 'user';
              size = 25;
            } else if (route.name === 'Device') {
              iconName = focused ? 'plus-square' : 'plus-square';
              size = 25;
              iconStyle = {marginRight: 10};
            }

            // You can return any component that you like here!
            return <Icons name={iconName} size={size} color={color} />;
            // return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: '#2866CD',
          inactiveTintColor: 'gray',
        }}
      > 
        <Tab.Screen name="User" component={AdimActivity} />
        <Tab.Screen name="Device" component={DeviceType} />
        <Tab.Screen name="Profile" component={AdminProfileActivity} />
      </Tab.Navigator>
    );
  }
}

class Group extends Component {
  render () {
    return (
      <Stack.Navigator>

        <Stack.Screen name="Group" component={DeviceNameActivity} />
        <Stack.Screen name="Device" component={DeviceActivity} />
        <Stack.Screen name="Settime" component={SettimeActivity} />
      </Stack.Navigator>
    );
  }
}

class Login extends Component {
  render () {
    return (
      <Stack.Navigator>

        <Stack.Screen name="Login" component={LoginActivity} />
        <Stack.Screen name="Register" component={RegisterActivity} />

      </Stack.Navigator>
    );
  }
}
class Main extends Component {
  render () {
    return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            let iconStyle;

            if (route.name === 'Main') {
              iconName = focused ? 'home' : 'home';
              size = 25;
            } else if (route.name === 'Camera') {
              iconName = focused ? 'camera' : 'camera';
              size = 20;
            } else if (route.name === 'Profile') {
              iconName = focused ? 'user' : 'user';
              size = 25;
            } else if (route.name === 'Device') {
              iconName = focused ? 'plus-square' : 'plus-square';
              size = 25;
              iconStyle = {marginRight: 10};
            }

            // You can return any component that you like here!
            return <Icons name={iconName} size={size} color={color} />;
            // return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: '#2C8D5D',
          inactiveTintColor: 'gray',
        }}
      >

        <Tab.Screen name="Main" component={MainActivity} />
        <Tab.Screen name="Camera" component={CameraActivity} />
        <Tab.Screen name="Profile" component={ProfileActivity} />
        <Tab.Screen name="Device" component={GroupActivity} />
      </Tab.Navigator>
    );
  }
}

class App extends Component {
  constructor () {
    super ();
    this.state = {
      user_token: null,
      get_user: null,
    };
  }
  render () {
    // console.log ('user_token 111111 : ', user_token);
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: null,
          }}
        >
          <Stack.Screen name="Splash" component={Splash} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Main" component={Main} />
          <Stack.Screen name="Group" component={Group} />
          <Stack.Screen name="Admin" component={Admin} />
          <Stack.Screen name="SeeMore" component={SeeMore} />
          <Stack.Screen name="MoreDevice" component={MoreDevice} />
          <Stack.Screen name="DeviceData" component={DeviceDataActivity} />
          <Stack.Screen name="EditDevice" component={EditDeviceActivity} />
          <Stack.Screen name="AddDevice" component={AddDevice} />
        </Stack.Navigator>
        
      </NavigationContainer>
    );
  }
}

export default App;
